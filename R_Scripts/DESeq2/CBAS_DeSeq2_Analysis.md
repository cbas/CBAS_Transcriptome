------------------------------------------------------------------------

## Preliminary remarks

The common blue aquarium sponge is a cyanosponge commonly found in salt
water aquaria. Most aquarium hobbiest refer to it as *Collospongia
auris*, however its taxonomic affiliation is not clear. I refer to this
sponge as the “Common Blue Aquarium Sponge”, **CBAS** in short. CBAS is
a cyanosponge, it harbours cyanobacteria from the Candidatus
*Synechococcus spongiarum* clade.

Culture of the sponge under different light conditions resulted in the
observation that when kept under dark conditions the sponge bleaches,
turning completely white after ca. 12 weeks under darkness; the “normal”
color of the sponge varies between green, blue and violet. Changes in
color are accompanied by changes in size and general morphology. For
instance, the sponges form elongated projections; the normal shape of
the sponge is lobated and plate like.

It is not clear whether the cyanobacteria are still present in the
bleached sponge tissues. Bioanalyzer profiles of RNA extracts from
bleached sponges clearly showed that the samples lack the rRNA bands
belonging the cyanobacteria but qPCR experiments pointed towards the
presence of cyanobacteria in DNA extracts from sponges exposed to max. 6
weeeks darkeness [1]. However, this evidence is (in my opinion) not
conclusive due to a number of technical problems associated to these
experiments; RNA extractions only demonstrate that the symbionts are not
active and the qPCR experiments suffered from incosistent amplifying of
the samples. Another piece of evidence (weakly) pointing towards this is
the capacity of the sponges to regain color after 8-12 weeks re-exposure
to light; symbiont uptake from the water column cannot be excluded,
however, apparently *S. spongiarum* is not present in the water. All in
all, the evidence at hand point to the presence of the symbionts in some
kind of inactive (transcriptionl/metabolic) state in the sponge tissues
exposed to dark conditions.

## Study rational

Since it is possible to manipulate the trascriptional/metabolic state of
the cyanobacterial symbionts through the exposure of the system to
darkness, CBAS represents an interesting system to assess how the sponge
reacts to symbiont inactivation. I use RNA-Seq data to *de novo*
assemble a reference transcriptome for this system and to assess changes
in the sponge gene expression profiles associated with different
symbiont transcriptional states. I hope these data contributes to our
understanding of the molecular mechanisms used by sponges to interact
with their microbiomes.

------------------------------------------------------------------------

# Introduction

# Materials and Methods

## Sponge culture and experimental setting

Sponges are kept in a 200L salt water aquarium under a 12hr light:12hr
darkness regime using two T5 fluorescent lights. For all experiments one
large sponge was used and a hole-puncher was used to produce explants of
10 mm diameter [2]. The explants were place in a recipient with sand and
were allow to recover for 3-5 days under control light conditions.
Recovery was visually assesed by inspecting the border of the explants
to corroborate the tissue healed at the cutting points. After this, the
explants were moved to a section of the aquarium covered with a two
sheets of black plastic cloth. The plastic cloth serves to block the
light from the T5 lights; the amount of light (in klux) penetrating the
plastic sheets was ~0 klux while under normal conditions at least ~15
klux can be measured at the water surface. Twelve weeks after moving the
sponges to the dark side of the aquarium, the explants were flash-frozen
in liquid nitrogen and kept at -80 °C; at this point in time explants
kept under control conditions were also sampled. This experiment was
repeated several times[3]; the resulting tissue samples were used to
test different RNA extraction protocols and assess whether bleaching can
be reproduced in the aquarium.

## RNA extraction, libray preparation and sequencing

Total RNA was extracted using a hybrid protocol that combines a regular
CTAB extraction and a spin-column clean-up step. Briefly, using mortar
and pestle, samples were pulverized in liquid nitrogen. The tissue
powder was lysed in 600 *μ*L warm (56 °C) CTAB-PVP-NaCl buffer
containing *β*-mercaptoethanol for 15-20 minutes with agitation (~500
rpm). After lysis, one volume acidic Phenol-Chloroform-Isoamyl alcohol
(25:24:1) was added to the samples and the extraction was vigorously
agitated until the liquid had a milky appearance. The samples were then
centrifuged at 14,000 rpm for 15 minutes to separate the phases. 400
*μ*L of the polar phase were then transfered to a new 2mL
microcentrifuge tube and the nucleic-acids were precipitated for 10
minutes using one volume isopropanol. The nucleic acids were recovered
by centrifugation (14,000rpm for 20 minutes at ~16°C) and the recovered
pellets were washed twice with 1 mL cold 80% ethanol. After washing,
ethanol droplets were removed with a 10 *μ*L pipette and the pellets
were air dried for ~10 minute before resuspending in 30 *μ*L
nuclease-free water.

After this initial extraction, the ZR-Duet DNA/RNA MiniPrep [4] was
used, following the manufacturers recommendations, to separate DNA and
RNA from the sample. The RNA was resuspended in 30 *μ*L nuclease-free
water and quality checked initially on 1% agarose gels and finally on a
Bioanalyzer 2100 Nano RNA chip. RNA concentration was measured on a
Nanodrop 1000. RIN values could be calculated for bleached samples only;
control samples have four rRNA peaks corresponding to the 16S, 18S, 23S
and 28S rRNA fragments and the Bioanalyzer cannot calculate their RIN
value. Quality of the control samples was assessed by overlaying
bleached samples with RIN and control samples without RIN and assessing
their similarity in terms of peak height and baseline level. Five
control and four bleached samples were sent on dry-ice to the EMBL
Genomics Core Facility [5] where they were used to produce
strand-specific libraries with ~110 base pairs (bp). These libraries
were multiplexed and pair-end sequenced (50bp reads) in two lanes of a
HiSeq 2500 (Illumina).

## Transcriptome assembly, annotation

Reads were quality controled using FastQC [6] and quality filtered using
the BioLite program filter\_illumina.cpp [7]. The surviving read pairs
from all libraries were used to produce (using the command cat) two
fastq files that were used for *de novo* transcriptome assembly in
Trinity v2.0.6 (using the –normalize\_reads flag). The resulting contigs
(with lenght &gt;=200bp) were annotated against the Uniprot (SwissProt)
[8] and *Amphimedon queenslandica* isoforms (Aqu2 proteins) [9] using
blastx v2.2.29+ with an expectation cutoff of 0.001. Only the best match
per contig was saved and the blast results were saved using blast’s XML
format converted to a 25 column table [10]. Both tables
(i.e. [UNIPROT](https://gitlab.lrz.de/cbas/CBAS_Transcriptome/-/tree/master/Annotations/Uniprot)
and
[Aqu2](https://gitlab.lrz.de/cbas/CBAS_Transcriptome/-/tree/master/Annotations/AQU2))
are available for download in the [project
repository](https://gitlab.lrz.de/cbas/CBAS_Transcriptome). Gene
Ontology [11] annotations for the CBAS transcriptome were obtained by
programmatically querying the [QuickGO
Webservice](http://www.ebi.ac.uk/QuickGO/) with the transcriptome’s
UNIPROT annotations [12] and a custom [perl
script](https://gitlab.lrz.de/cbas/CBAS_Transcriptome/-/tree/master/Scripts/Get_GO_Annotations.pl).
For each CBAS transcript the “component”, “function” and “process” GO
terms associated with its UNIPROT best match were stored in the [project
repository](https://gitlab.lrz.de/cbas/CBAS_Transcriptome/-/tree/master/Annotations/GOs)
as independent tab-separated files that can be easily modified to use as
input files in TopGO [13].

In addition, transcripts were translated using the program
TransDecoder.LongOrfs [14] and the resulting cds, mRNA, bed, gff3 and
pep files stored in the [project
repository](https://gitlab.lrz.de/cbas/CBAS_Transcriptome/-/tree/master/Annotations/Transdecoder)
and used to annotate the transcriptome against the
[Pfam](http://pfam.xfam.org/) and [KEEG](http://www.genome.jp/kegg/)
databases. For this, the perl script
[pfam\_scan.pl](ftp://ftp.ebi.ac.uk/pub/databases/Pfam/Tools/) and the
webservice [BlastKOALA](http://www.kegg.jp/blastkoala/) were used; the
resulting output files can be found in the [project
repository.](https://gitlab.lrz.de/cbas/CBAS_Transcriptome) Finally, the
assembled transcripts were blasted (blastn) against a bacterial genomes
database [15].

All annotations were used to build an [annotation
meta-table](https://gitlab.lrz.de/cbas/CBAS_Transcriptome/-/tree/master/Annotations/Metatable/)
using a [custom made perl
script](https://gitlab.lrz.de/cbas/CBAS_Transcriptome/-/tree/master/Scripts/Create_Transcriptome_Annotation_Table.pl)
available in the project repository.

## Transcriptome completeness assessment

The CBAS transcriptome completeness was assessed by blasting against the
CEGMA gene set of [Parra et
al. 2007](http://bioinformatics.oxfordjournals.org/content/23/9/1061.abstract)[16]
using tblastn with an e-value of 1*e*<sup>−</sup>19 as implemented in
the scrtipt
[find\_cegma\_genes.py](https://bitbucket.org/wrf/galaxy-files/src). For
details about the method see [17].

## Differential gene expression analysis

Using the *de novo* assembled transcriptome, the individual libraries
(i.e. control and bleached sponges) were mapped with RSEM and a
transcript by sample count matrix was derived from the “gene” counts
with the program
[abundance\_estimates\_to\_matrix.pl](https://gitlab.lrz.de/cbas/CBAS_Transcriptome/-/tree/master/Counts/RSEM)
provided as part of the Trinity package [18]. The [count
matrix](https://gitlab.lrz.de/cbas/CBAS_Transcriptome/-/tree/master/Counts/)
was used to find differentially expressed genes in control *vs.*
bleached sponges (model = ~Treatment). For this, the package
[DeSeq2](https://bioconductor.org/packages/release/bioc/html/DESeq2.html)
was used. In brief, transcripts with 0 counts over all samples were
removed from the matrix and Principal Component Analysis was used to
assess the global expression pattern of control *vs.* bleached sponges
and identify potential outliers. Size effects and dispersions were
estimated with the DeSe2 methods **estimateSizeFactors** and
**estimateDispersions** and differentially expressed transcripts were
then infered using a Wald test (DeSeq2 method **nbinomWaldTest**). The
resulting p-values were adjusted using the Benjamin-Hochberg correction.
Transcripts with **log fold change  &lt;  − 2** or **log fold change
 &gt; 2** and **adjusted p-value  &lt; 0.01** were considered as
differentially expressed for further analyses.

A stand-alone R script to replicate the analysis is available in the
[project
repository](https://gitlab.lrz.de/cbas/CBAS_Transcriptome/-/tree/master/Scripts)
and can be run using the [count
matrix](https://gitlab.lrz.de/cbas/CBAS_Transcriptome/-/tree/master/Counts/CBAS_Bleaching_RSEM_Expression_Matrix.counts)
and [sample
information](https://gitlab.lrz.de/cbas/CBAS_Transcriptome/-/tree/master/Counts/CBAS_Bleaching_RSEM_Expression_Matrix.info)
provided. And in this version of the manuscript, the code used for
several calculations is provided embedded as Rmarkdown snippets in the
source code of this document.

## Gene Ontology Term enrichment analysis

In order to assess whether certain Gene Ontology terms are “enriched”
among the set of differentially expressed genes a GO-term enrichment
analysis was done using the R package
[TopGO](https://bioconductor.org/packages/release/bioc/html/topGO.html).
For this analysis, “background sets” of not differentially expressed
transcripts with similar expression profile to the set of differentially
expressed transcripts were created using the method **genefinder** of
the package
[genefilter](http://bioconductor.org/packages/release/bioc/html/genefilter.html)
with the Manhattan distance. Background transcript sets were created for
all differentially expressed transcripts (i.e. transcripts with log fold
change  &gt; |2| and *p* &lt; 0.01), for differentially upregulated
genes (i.e. transcripts with log fold change  &gt; 2 and *p* &lt; 0.01)
and for differentially downregulated genes (i.e. transcripts with log
fold change  &lt;  − 2 and *p* &lt; 0.01). The background sets were then
used to test for enrichment of certain GO-terms in the global set of
differentially expressed transcripts (log fold change  &gt; |2| and
*p* &lt; 0.01), the set of upregulated genes (log fold change  &gt; 2
and *p* &lt; 0.01) and the set of downregulated genes (log fold change
 &lt;  − 2 and *p* &lt; 0.01). For the enrichment analysis, a Fisher
exact test with the *classic* algorithm implemented in the method
**runTest** of
[TopGo](https://bioconductor.org/packages/release/bioc/html/topGO.html)
was used.

## Pfam domain enrichment analysis

In addition to the GO-Term enrichment analysis, and due to the
difficulties in assigning a meaning to the GO annotations in our sponge
model, a Pfam domain enrichment analysis was also done. Essentially the
Pfam enrichment analysis works in a similar way that the GO-term
enrichment analysis. First, a background set of genes not differentially
expressed but showing similar expression patterns than the DEGs is
calculated and used to provide a population of transcripts against which
the differentially expressed transcripts can be compared. Once the
background population of transcripts is available, the frequency with
which a given domain is found in the background set of transcripts is
compared with the frequency with which the same domain is found in the
set of differentially expressed transcripts using an hypergeometric
test. To account for multiple comparisons, the p-values are adjusted
using the Benjamini-Hochberg correctino. The background distribution
used for the Pfam enrichment domains was the same used for the analysis
of GO-term enrichment.

# Results

## The transcriptome of the Common Blue Aquarium Sponge, a model cyanosponge

Per library, we obtained on average 26,385,834 ( ± 3, 360, 778) pairs of
reads (50 bp long). After cleaning, each library had an average of
24,942,084 ( ± 3, 420, 083) surviving pairs. The concatenated dataset
thus had 199,536,668 pairs. Table 1 contains the information on the
number of sequenced and surviving reads per library.

**Table 1.** Reads by library before and after quality filtering.

<table>
<thead>
<tr class="header">
<th style="text-align: center;">Library</th>
<th style="text-align: center;">Condition</th>
<th style="text-align: center;">Sequenced reads</th>
<th style="text-align: center;">Reads post cleaning</th>
<th style="text-align: center;">% Kept</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: center;">1</td>
<td style="text-align: center;">Control</td>
<td style="text-align: center;">33,836,615</td>
<td style="text-align: center;">32,397,726</td>
<td style="text-align: center;">95.75</td>
</tr>
<tr class="even">
<td style="text-align: center;">2</td>
<td style="text-align: center;">Control</td>
<td style="text-align: center;">26,740,977</td>
<td style="text-align: center;">25,252,825</td>
<td style="text-align: center;">94.43</td>
</tr>
<tr class="odd">
<td style="text-align: center;">3</td>
<td style="text-align: center;">Control</td>
<td style="text-align: center;">22,823,936</td>
<td style="text-align: center;">21,016,976</td>
<td style="text-align: center;">92.08</td>
</tr>
<tr class="even">
<td style="text-align: center;">4</td>
<td style="text-align: center;">Control</td>
<td style="text-align: center;">24,166,051</td>
<td style="text-align: center;">22,655,601</td>
<td style="text-align: center;">93.75</td>
</tr>
<tr class="odd">
<td style="text-align: center;">5</td>
<td style="text-align: center;">Bleached</td>
<td style="text-align: center;">26,579,563</td>
<td style="text-align: center;">25,067,523</td>
<td style="text-align: center;">94.31</td>
</tr>
<tr class="even">
<td style="text-align: center;">6</td>
<td style="text-align: center;">Bleached</td>
<td style="text-align: center;">25,448,522</td>
<td style="text-align: center;">24,229,217</td>
<td style="text-align: center;">95.21</td>
</tr>
<tr class="odd">
<td style="text-align: center;">7</td>
<td style="text-align: center;">Bleached</td>
<td style="text-align: center;">22,823,936</td>
<td style="text-align: center;">21,016,976</td>
<td style="text-align: center;">92.08</td>
</tr>
<tr class="even">
<td style="text-align: center;">8</td>
<td style="text-align: center;">Bleached</td>
<td style="text-align: center;">24,166,051</td>
<td style="text-align: center;">22,655,601</td>
<td style="text-align: center;">93.75</td>
</tr>
<tr class="odd">
<td style="text-align: center;">Total Reads</td>
<td style="text-align: center;"></td>
<td style="text-align: center;">211,086,672</td>
<td style="text-align: center;">199,536,668</td>
<td style="text-align: center;">94.53</td>
</tr>
</tbody>
</table>

------------------------------------------------------------------------

*De novo* transcriptome assembly using the concatenated dataset resulted
in 128,686 transcript  &gt; 200bp. The N50 of the transcriptome was
1,281 bp, and the median lenght of the assembled transcripts was 453 bp
(MAD=0.2078891). More details about the assembly can be found in Table 2
and the length distribution of the transcripts can be visualized in
Figure 1.

**Table 2.** Statistics for the *de novo* assembled CBAS transcriptome

<table>
<thead>
<tr class="header">
<th style="text-align: center;">Statistic</th>
<th style="text-align: center;">Obtained value</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: center;">GC content</td>
<td style="text-align: center;">40.0</td>
</tr>
<tr class="even">
<td style="text-align: center;">N50</td>
<td style="text-align: center;">1,281</td>
</tr>
<tr class="odd">
<td style="text-align: center;">Max. length</td>
<td style="text-align: center;">19,309</td>
</tr>
<tr class="even">
<td style="text-align: center;">Mean length</td>
<td style="text-align: center;">803</td>
</tr>
<tr class="odd">
<td style="text-align: center;">Median length</td>
<td style="text-align: center;">453</td>
</tr>
<tr class="even">
<td style="text-align: center;">Min. length</td>
<td style="text-align: center;">224</td>
</tr>
<tr class="odd">
<td style="text-align: center;">Number of Transcripts</td>
<td style="text-align: center;">128,686</td>
</tr>
</tbody>
</table>

------------------------------------------------------------------------

<img src="CBAS_DeSeq2_Analysis_files/figure-markdown_strict/transcript_length_fig-1.png" style="display: block; margin: auto;" />

**Figure 1.** Transcript length distribution of CBAS reference
transcriptome. Maximum length allowed equal 10,000 base-pairs.

------------------------------------------------------------------------

Of the collection of transcripts, 37.75% could be translated into
proteins by Transdecoder. The majority of the translated transcripts
were *Complete ORFs*, according to Transdecoder. The *Transdecoder ORF
Types* distribution of the translated transcripts can be found in Table
3.

**Table 3.** *Transdecoder ORF Type* of the translated transcripts.

<table>
<thead>
<tr class="header">
<th style="text-align: center;">ORF Type</th>
<th style="text-align: center;">Count</th>
<th style="text-align: center;">%</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: center;">3’ partial</td>
<td style="text-align: center;">6734</td>
<td style="text-align: center;">13.86</td>
</tr>
<tr class="even">
<td style="text-align: center;">5’ partial</td>
<td style="text-align: center;">10535</td>
<td style="text-align: center;">21.69</td>
</tr>
<tr class="odd">
<td style="text-align: center;">Internal</td>
<td style="text-align: center;">13473</td>
<td style="text-align: center;">27.74</td>
</tr>
<tr class="even">
<td style="text-align: center;">Complete</td>
<td style="text-align: center;">17833</td>
<td style="text-align: center;">36.71</td>
</tr>
<tr class="odd">
<td style="text-align: center;">Total</td>
<td style="text-align: center;">48575</td>
<td style="text-align: center;">100</td>
</tr>
</tbody>
</table>

------------------------------------------------------------------------

Regarding the annotation of the translated transcripts [19], 29.44% had
a matching *Uniprot* annotation. The number of transcripts matching an
*Amphimedon queenslandica* (Aqu2) protein was slightly higher, 36.21%.
*Gene Ontology Component*, *Function* and *Process* annotations could be
retrieved for 26.3%, 26.49% and 26.76% of the transcripts, respectively.
Additionally, 23.45% of the transcripts could be annotated with *Pfam*
domain information. In contrast, only 5.8% of the assembled transcripts
could be annotated against the *KEGG* database.

<img src="CBAS_DeSeq2_Analysis_files/figure-markdown_strict/annotation_success_fig-1.png" style="display: block; margin: auto;" />
**Figure 2.** Annotation success by database for transcripts translated
by Transdecoder.

------------------------------------------------------------------------

Regarding annotation success, different *Transdecoder ORF Types* had
different annotation success rates (Fig. 3) with *complete ORFs* having
higher annotation sucess than other *ORF types* and *3’ Partial ORFs*
having the lowest annotation success.

<img src="CBAS_DeSeq2_Analysis_files/figure-markdown_strict/annotation_count_byDB_Fig-1.png" style="display: block; margin: auto;" />
**Figure 3.** Annotation count by database for different types of ORF as
defined by Transdecoder.

------------------------------------------------------------------------

For annotation against Uniprot and Aqu2 we used a permissive evalue
(0.001) to query these databases. Thus, we risk obtaining annotations
that are only partial matches or that are of doubtful homology. Despite
our permissive annotation strategy, the mean evalues obtained for the
annotations against Uniprot and Aqu2 had clearly lower values than the
threshold set
(i.e. 2.519351 × 10<sup>−7</sup> ± 1.127118 × 10<sup>−6</sup> and
2.4146826 × 10<sup>−7</sup> ± 1.1139696 × 10<sup>−6</sup>,
respectively). Interestintly, the mean evalues obtained for both Uniprot
and Aqu2 annotations were similar for all *Transdecoder ORF types*
(Table 4) and the maximum evalue obtained after querying these two
databases was 1*x*10<sup>−05</sup> [20].

**Table 4.** Mean evalue by Transdecoder ORF type for annotations
obtained from the Uniprot or Aqu2 databases.

<table>
<colgroup>
<col style="width: 22%" />
<col style="width: 25%" />
<col style="width: 51%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: center;">Transdecoder ORF Type</th>
<th style="text-align: center;">Database</th>
<th style="text-align: center;">Mean evalue (SD)</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: center;">3’ partial</td>
<td style="text-align: center;">Uniprot</td>
<td style="text-align: center;"><span
class="math inline">2.1738267 × 10<sup>−7</sup></span> (<span
class="math inline">1.0499955 × 10<sup>−6</sup></span>)</td>
</tr>
<tr class="even">
<td style="text-align: center;">5’ partial</td>
<td style="text-align: center;">Uniprot</td>
<td style="text-align: center;"><span
class="math inline">2.1534162 × 10<sup>−7</sup></span> (<span
class="math inline">1.0780619 × 10<sup>−6</sup></span>)</td>
</tr>
<tr class="odd">
<td style="text-align: center;">Internal</td>
<td style="text-align: center;">Uniprot</td>
<td style="text-align: center;"><span
class="math inline">4.9614047 × 10<sup>−7</sup></span> (<span
class="math inline">1.5576029 × 10<sup>−6</sup></span>)</td>
</tr>
<tr class="even">
<td style="text-align: center;">Complete</td>
<td style="text-align: center;">Uniprot</td>
<td style="text-align: center;"><span
class="math inline">1.6779664 × 10<sup>−7</sup></span> (<span
class="math inline">8.9792016 × 10<sup>−7</sup></span>)</td>
</tr>
<tr class="odd">
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;">3’ partial</td>
<td style="text-align: center;">Aqu2</td>
<td style="text-align: center;"><span
class="math inline">2.2338358 × 10<sup>−7</sup></span> (<span
class="math inline">1.0988336 × 10<sup>−6</sup></span>)</td>
</tr>
<tr class="odd">
<td style="text-align: center;">5’ partial</td>
<td style="text-align: center;">Aqu2</td>
<td style="text-align: center;"><span
class="math inline">1.8405011 × 10<sup>−7</sup></span> (<span
class="math inline">9.6965115 × 10<sup>−7</sup></span>)</td>
</tr>
<tr class="even">
<td style="text-align: center;">Internal</td>
<td style="text-align: center;">Aqu2</td>
<td style="text-align: center;"><span
class="math inline">4.5642628 × 10<sup>−7</sup></span> (<span
class="math inline">1.5267768 × 10<sup>−6</sup></span>)</td>
</tr>
<tr class="odd">
<td style="text-align: center;">Complete</td>
<td style="text-align: center;">Aqu2</td>
<td style="text-align: center;"><span
class="math inline">1.6698938 × 10<sup>−7</sup></span> (<span
class="math inline">9.0483509 × 10<sup>−7</sup></span>)</td>
</tr>
</tbody>
</table>

------------------------------------------------------------------------

Finally, in terms of completeness, we recovered 243 out of 248 KOGs
(i.e. 97.98%) from the CBAS transcriptome. Yet, the number of
transcripts classified as *High-confidence, full length matches* was
only 175 (i.e. 70.56%). In addition, six transcripts were categorized as
*Probable full length matches*, six more were tagged as matching a KOG
that was slightly shorter than the query and 4 transcripts matched a KOG
that was much longer than the query. Thus, in total 191 (i.e. 77.02%)
transcripts can be considered high confidence KOG matches. Transcripts
matching KOGs that were much shorter and transcripts probably
representing missassemblies amount to 22 and 30, respectively
(i.e. 20.97%).

## Global gene expression patterns change due to symbiont inactivation

The count matrix used to investigate changes in gene expression in
Bleached *vs.* Control sponges had 100098 rows with counts. Of these,
however, only 52567 had counts in all samples (i.e. the transcript was
detected in all sequenced libraries). The inspection of the cummulative
density distribution and of the density distribution of normalized
counts (Fig. 5) for either all rows (not shown) or only the non-zero
rows in the matrix showed similar patterns for all libraries indicating
that they were sequenced at similar depths and can be compared safely.

<img src="CBAS_DeSeq2_Analysis_files/figure-markdown_strict/normalized_counts_cumulativeDist_qc_figure-1.png" style="display: block; margin: auto;" />
**Figure 5.** Cummulative density distribution of normalized counts and
density distribution of normalized counts of RNA-Seq libraries prepared
samples for *bleached* and *control* **CBAS** samples.

------------------------------------------------------------------------

A pairwise comparison of the individual libraries (Fig 6.) provided
further evidence for the technical similarity of the libraries.

<img src="CBAS_DeSeq2_Analysis_files/figure-markdown_strict/MDPlots_library_01_qc_figure-1.png" style="display: block; margin: auto;" />

<img src="CBAS_DeSeq2_Analysis_files/figure-markdown_strict/MDPlots_library_02_qc_figure-1.png" style="display: block; margin: auto;" />

<img src="CBAS_DeSeq2_Analysis_files/figure-markdown_strict/MDPlots_library_03_qc_figure-1.png" style="display: block; margin: auto;" />

<img src="CBAS_DeSeq2_Analysis_files/figure-markdown_strict/MDPlots_library_04_qc_figure-1.png" style="display: block; margin: auto;" />

<img src="CBAS_DeSeq2_Analysis_files/figure-markdown_strict/MDPlots_library_05_qc_figure-1.png" style="display: block; margin: auto;" />

<img src="CBAS_DeSeq2_Analysis_files/figure-markdown_strict/MDPlots_library_06_qc_figure-1.png" style="display: block; margin: auto;" />

<img src="CBAS_DeSeq2_Analysis_files/figure-markdown_strict/MDPlots_library_07_qc_figure-1.png" style="display: block; margin: auto;" />

<img src="CBAS_DeSeq2_Analysis_files/figure-markdown_strict/MDPlots_library_08_qc_figure-1.png" style="display: block; margin: auto;" />
**Figure 6.** Pairwise library comparisons based on the relation between
the *Mean log-fold* and the *Mean (log-transformed) counts*. In general,
no trend should be observed in the pairwise comparisons.

------------------------------------------------------------------------

Libraries were also compared in terms of the *FPKM* and *TPM*
distribution of the transcripts (Fig. 7). This analysis revealed that
two libraries (i.e. CBAS\_Control\_2 and CBAS\_Control\_3) differed
markedly from all other sequenced libraries. Thus, we restricted the
count matrix to include only *trinity genes* that could be translated
using Transdecoder (29,705 transcripts) . After this, the libraries were
comparable in terms of the distribution of TPM and FPKM values. The
analysis of differential expression was conducted on this reduced data
matrix.

<img src="CBAS_DeSeq2_Analysis_files/figure-markdown_strict/TPM_figure-1.png" style="display: block; margin: auto;" />
**Figure 7.** TPM and FPKM distribution in datasets including all
trinity genes (All transcripts) and only trinity genes that could be
translated with trandescoder (Transdecoded transcripts). In general,
libraries should have similar TPM/FPKM distributions to be comparable.

------------------------------------------------------------------------

For the analysis of differentially expressed genes, the count matrix was
transformed using the *rlog* transformation available in the R package
DeSeq2. A cluster analysis based on the between-sample distances
calculated using the *rlog* transformed counts grouped samples in two
groups matching the *control* and *treatment* groups. Interestingly one
treatment sample behaved in an anomalous manner, clustering with neither
the treatment nor the control group. This sample
(i.e. CBAS\_Bleached\_5) was also found to be different in a Principal
Component Analysis (PCA) conducted on the *rlog* transformed count
matrix.

------------------------------------------------------------------------

<img src="CBAS_DeSeq2_Analysis_files/figure-markdown_strict/sample_heat_pca_plot-1.png" style="display: block; margin: auto;" /><img src="CBAS_DeSeq2_Analysis_files/figure-markdown_strict/sample_heat_pca_plot-2.png" style="display: block; margin: auto;" />

**Figure 9.** Cluster and Principal Component Analysis of the *rlog*
transformed counts for *bleached vs. control* CBAS specimens.

------------------------------------------------------------------------

If sample *CBAS\_Bleached\_5* is removed from the analysis, *bleached*
samples are grouped together while *control* sponges are divided in two
groups in both the cluster analysis and the principal component
analysis.

<img src="CBAS_DeSeq2_Analysis_files/figure-markdown_strict/drop_anomalous_sample_and_replot-1.png" style="display: block; margin: auto;" /><img src="CBAS_DeSeq2_Analysis_files/figure-markdown_strict/drop_anomalous_sample_and_replot-2.png" style="display: block; margin: auto;" />

**Figure 10.** Cluster and Principal Component Analysis of the *rlog*
transformed counts for *bleached vs. control* CBAS specimens after
removing sample *CBAS\_Bleached\_5*.

------------------------------------------------------------------------

We detected significant differences in the global expression patters
bewteen treated and control sponges (Adonis PseudoF = 5.1164, df = 1,
p-value = 0.039). In this reduced dataset, a total of 5503 transcripts
were differentially expressed between the *control* and *bleached*
sponges (Fig. 11). Of these transcripts, 1145 were overexpressed (with a
log2 fold change higher or equal to 1) in *bleached vs. control* sponges
and 2588 were expressed at lower levels (a log2 fold change lower or
equal than -1) in *bleached vs. control* specimens.

<img src="CBAS_DeSeq2_Analysis_files/figure-markdown_strict/MA_plot-1.png" style="display: block; margin: auto;" /><img src="CBAS_DeSeq2_Analysis_files/figure-markdown_strict/MA_plot-2.png" style="display: block; margin: auto;" />

**Figure 11.** MA-plot showing the relation between log-fold change and
mean count for each analyzed CBAS transcript. Transcripts significant at
0.01 are highlighted in red.

------------------------------------------------------------------------

It is worth noting that DeSeq2 was able to adecuately model the variance
in the dataset and could correctly calculate the p-values for the
analyzed transcripts (Fig 12).

<img src="CBAS_DeSeq2_Analysis_files/figure-markdown_strict/probability_plot-1.png" style="display: block; margin: auto;" />

**Figure 12.** Histogram of the p-values obtained for the transcripts
analyzed using DeSeq2. A uniform distribution between 0 and 1, with a
peak at zero, should be observed if the variance of the dataset could be
correctly modelled.

------------------------------------------------------------------------

## The expression of immune response related transcripts changes between symbiontic states

For Gene Ontology (GO) enrichment analyses we were able to select a
background set of transcripts showing similar expression patterns but no
significant change between conditions for all transcripts as well as for
overexpressed and underexpressed transcripts (Fig. 13). Enriched
Function, Process and Compartment GO terms for over- and underexpressed
genes can be found in Tables 5-10.

<img src="CBAS_DeSeq2_Analysis_files/figure-markdown_strict/plot_back_vs_forground_dist-1.png" style="display: block; margin: auto;" />

Table 5: GO-Term functions overrepresented in the set of overexpressed
DEGs.

<table>
<colgroup>
<col style="width: 3%" />
<col style="width: 11%" />
<col style="width: 45%" />
<col style="width: 10%" />
<col style="width: 12%" />
<col style="width: 9%" />
<col style="width: 8%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: left;"></th>
<th style="text-align: left;">GO.ID</th>
<th style="text-align: left;">Term</th>
<th style="text-align: right;">Annotated</th>
<th style="text-align: right;">Significant</th>
<th style="text-align: right;">Expected</th>
<th style="text-align: left;">classic</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">10</td>
<td style="text-align: left;"><a href="GO:0004197"
class="uri">GO:0004197</a></td>
<td style="text-align: left;">cysteine-type endopeptidase activity</td>
<td style="text-align: right;">21</td>
<td style="text-align: right;">14</td>
<td style="text-align: right;">5.88</td>
<td style="text-align: left;">0.00024</td>
</tr>
<tr class="even">
<td style="text-align: left;">11</td>
<td style="text-align: left;"><a href="GO:0004843"
class="uri">GO:0004843</a></td>
<td style="text-align: left;">thiol-dependent deubiquitinase</td>
<td style="text-align: right;">15</td>
<td style="text-align: right;">11</td>
<td style="text-align: right;">4.20</td>
<td style="text-align: left;">0.00033</td>
</tr>
<tr class="odd">
<td style="text-align: left;">12</td>
<td style="text-align: left;"><a href="GO:0101005"
class="uri">GO:0101005</a></td>
<td style="text-align: left;">deubiquitinase activity</td>
<td style="text-align: right;">15</td>
<td style="text-align: right;">11</td>
<td style="text-align: right;">4.20</td>
<td style="text-align: left;">0.00033</td>
</tr>
<tr class="even">
<td style="text-align: left;">13</td>
<td style="text-align: left;"><a href="GO:0016787"
class="uri">GO:0016787</a></td>
<td style="text-align: left;">hydrolase activity</td>
<td style="text-align: right;">462</td>
<td style="text-align: right;">158</td>
<td style="text-align: right;">129.41</td>
<td style="text-align: left;">0.00045</td>
</tr>
<tr class="odd">
<td style="text-align: left;">14</td>
<td style="text-align: left;"><a href="GO:0019783"
class="uri">GO:0019783</a></td>
<td style="text-align: left;">ubiquitin-like protein-specific
protease…</td>
<td style="text-align: right;">18</td>
<td style="text-align: right;">12</td>
<td style="text-align: right;">5.04</td>
<td style="text-align: left;">0.00069</td>
</tr>
<tr class="even">
<td style="text-align: left;">15</td>
<td style="text-align: left;"><a href="GO:0035375"
class="uri">GO:0035375</a></td>
<td style="text-align: left;">zymogen binding</td>
<td style="text-align: right;">19</td>
<td style="text-align: right;">12</td>
<td style="text-align: right;">5.32</td>
<td style="text-align: left;">0.00139</td>
</tr>
<tr class="odd">
<td style="text-align: left;">16</td>
<td style="text-align: left;"><a href="GO:0008242"
class="uri">GO:0008242</a></td>
<td style="text-align: left;">omega peptidase activity</td>
<td style="text-align: right;">17</td>
<td style="text-align: right;">11</td>
<td style="text-align: right;">4.76</td>
<td style="text-align: left;">0.00168</td>
</tr>
<tr class="even">
<td style="text-align: left;">17</td>
<td style="text-align: left;"><a href="GO:0005200"
class="uri">GO:0005200</a></td>
<td style="text-align: left;">structural constituent of
cytoskeleton</td>
<td style="text-align: right;">29</td>
<td style="text-align: right;">16</td>
<td style="text-align: right;">8.12</td>
<td style="text-align: left;">0.00175</td>
</tr>
<tr class="odd">
<td style="text-align: left;">18</td>
<td style="text-align: left;"><a href="GO:0043167"
class="uri">GO:0043167</a></td>
<td style="text-align: left;">ion binding</td>
<td style="text-align: right;">963</td>
<td style="text-align: right;">298</td>
<td style="text-align: right;">269.74</td>
<td style="text-align: left;">0.00204</td>
</tr>
<tr class="even">
<td style="text-align: left;">19</td>
<td style="text-align: left;"><a href="GO:0017111"
class="uri">GO:0017111</a></td>
<td style="text-align: left;">nucleoside-triphosphatase activity</td>
<td style="text-align: right;">105</td>
<td style="text-align: right;">43</td>
<td style="text-align: right;">29.41</td>
<td style="text-align: left;">0.00224</td>
</tr>
<tr class="odd">
<td style="text-align: left;">20</td>
<td style="text-align: left;"><a href="GO:0140096"
class="uri">GO:0140096</a></td>
<td style="text-align: left;">catalytic activity, acting on a
protein</td>
<td style="text-align: right;">486</td>
<td style="text-align: right;">161</td>
<td style="text-align: right;">136.13</td>
<td style="text-align: left;">0.00227</td>
</tr>
<tr class="even">
<td style="text-align: left;">21</td>
<td style="text-align: left;"><a href="GO:0016462"
class="uri">GO:0016462</a></td>
<td style="text-align: left;">pyrophosphatase activity</td>
<td style="text-align: right;">110</td>
<td style="text-align: right;">44</td>
<td style="text-align: right;">30.81</td>
<td style="text-align: left;">0.00345</td>
</tr>
<tr class="odd">
<td style="text-align: left;">22</td>
<td style="text-align: left;"><a href="GO:0016818"
class="uri">GO:0016818</a></td>
<td style="text-align: left;">hydrolase activity, acting on acid
anhyd…</td>
<td style="text-align: right;">110</td>
<td style="text-align: right;">44</td>
<td style="text-align: right;">30.81</td>
<td style="text-align: left;">0.00345</td>
</tr>
<tr class="even">
<td style="text-align: left;">23</td>
<td style="text-align: left;"><a href="GO:0003735"
class="uri">GO:0003735</a></td>
<td style="text-align: left;">structural constituent of ribosome</td>
<td style="text-align: right;">52</td>
<td style="text-align: right;">24</td>
<td style="text-align: right;">14.57</td>
<td style="text-align: left;">0.00355</td>
</tr>
<tr class="odd">
<td style="text-align: left;">24</td>
<td style="text-align: left;"><a href="GO:0016817"
class="uri">GO:0016817</a></td>
<td style="text-align: left;">hydrolase activity, acting on acid
anhyd…</td>
<td style="text-align: right;">111</td>
<td style="text-align: right;">44</td>
<td style="text-align: right;">31.09</td>
<td style="text-align: left;">0.00422</td>
</tr>
<tr class="even">
<td style="text-align: left;">25</td>
<td style="text-align: left;"><a href="GO:0004175"
class="uri">GO:0004175</a></td>
<td style="text-align: left;">endopeptidase activity</td>
<td style="text-align: right;">155</td>
<td style="text-align: right;">58</td>
<td style="text-align: right;">43.42</td>
<td style="text-align: left;">0.00501</td>
</tr>
<tr class="odd">
<td style="text-align: left;">26</td>
<td style="text-align: left;"><a href="GO:0004715"
class="uri">GO:0004715</a></td>
<td style="text-align: left;">non-membrane spanning protein tyrosine
k…</td>
<td style="text-align: right;">19</td>
<td style="text-align: right;">11</td>
<td style="text-align: right;">5.32</td>
<td style="text-align: left;">0.00578</td>
</tr>
<tr class="even">
<td style="text-align: left;">27</td>
<td style="text-align: left;"><a href="GO:0005507"
class="uri">GO:0005507</a></td>
<td style="text-align: left;">copper ion binding</td>
<td style="text-align: right;">17</td>
<td style="text-align: right;">10</td>
<td style="text-align: right;">4.76</td>
<td style="text-align: left;">0.00731</td>
</tr>
<tr class="odd">
<td style="text-align: left;">28</td>
<td style="text-align: left;"><a href="GO:0042393"
class="uri">GO:0042393</a></td>
<td style="text-align: left;">histone binding</td>
<td style="text-align: right;">15</td>
<td style="text-align: right;">9</td>
<td style="text-align: right;">4.20</td>
<td style="text-align: left;">0.00921</td>
</tr>
<tr class="even">
<td style="text-align: left;">29</td>
<td style="text-align: left;"><a href="GO:0043130"
class="uri">GO:0043130</a></td>
<td style="text-align: left;">ubiquitin binding</td>
<td style="text-align: right;">15</td>
<td style="text-align: right;">9</td>
<td style="text-align: right;">4.20</td>
<td style="text-align: left;">0.00921</td>
</tr>
<tr class="odd">
<td style="text-align: left;">30</td>
<td style="text-align: left;"><a href="GO:0004497"
class="uri">GO:0004497</a></td>
<td style="text-align: left;">monooxygenase activity</td>
<td style="text-align: right;">21</td>
<td style="text-align: right;">11</td>
<td style="text-align: right;">5.88</td>
<td style="text-align: left;">0.01525</td>
</tr>
<tr class="even">
<td style="text-align: left;">31</td>
<td style="text-align: left;"><a href="GO:0032182"
class="uri">GO:0032182</a></td>
<td style="text-align: left;">ubiquitin-like protein binding</td>
<td style="text-align: right;">16</td>
<td style="text-align: right;">9</td>
<td style="text-align: right;">4.48</td>
<td style="text-align: left;">0.01592</td>
</tr>
<tr class="odd">
<td style="text-align: left;">32</td>
<td style="text-align: left;"><a href="GO:0004888"
class="uri">GO:0004888</a></td>
<td style="text-align: left;">transmembrane signaling receptor
activit…</td>
<td style="text-align: right;">100</td>
<td style="text-align: right;">38</td>
<td style="text-align: right;">28.01</td>
<td style="text-align: left;">0.01673</td>
</tr>
<tr class="even">
<td style="text-align: left;">33</td>
<td style="text-align: left;"><a href="GO:0038023"
class="uri">GO:0038023</a></td>
<td style="text-align: left;">signaling receptor activity</td>
<td style="text-align: right;">101</td>
<td style="text-align: right;">38</td>
<td style="text-align: right;">28.29</td>
<td style="text-align: left;">0.01987</td>
</tr>
<tr class="odd">
<td style="text-align: left;">34</td>
<td style="text-align: left;"><a href="GO:0060089"
class="uri">GO:0060089</a></td>
<td style="text-align: left;">molecular transducer activity</td>
<td style="text-align: right;">101</td>
<td style="text-align: right;">38</td>
<td style="text-align: right;">28.29</td>
<td style="text-align: left;">0.01987</td>
</tr>
<tr class="even">
<td style="text-align: left;">35</td>
<td style="text-align: left;"><a href="GO:0005201"
class="uri">GO:0005201</a></td>
<td style="text-align: left;">extracellular matrix structural
constitu…</td>
<td style="text-align: right;">14</td>
<td style="text-align: right;">8</td>
<td style="text-align: right;">3.92</td>
<td style="text-align: left;">0.02047</td>
</tr>
<tr class="odd">
<td style="text-align: left;">36</td>
<td style="text-align: left;"><a href="GO:0004713"
class="uri">GO:0004713</a></td>
<td style="text-align: left;">protein tyrosine kinase activity</td>
<td style="text-align: right;">54</td>
<td style="text-align: right;">22</td>
<td style="text-align: right;">15.13</td>
<td style="text-align: left;">0.02786</td>
</tr>
<tr class="even">
<td style="text-align: left;">37</td>
<td style="text-align: left;"><a href="GO:0035639"
class="uri">GO:0035639</a></td>
<td style="text-align: left;">purine ribonucleoside triphosphate
bindi…</td>
<td style="text-align: right;">408</td>
<td style="text-align: right;">130</td>
<td style="text-align: right;">114.28</td>
<td style="text-align: left;">0.02962</td>
</tr>
<tr class="odd">
<td style="text-align: left;">38</td>
<td style="text-align: left;"><a href="GO:0032553"
class="uri">GO:0032553</a></td>
<td style="text-align: left;">ribonucleotide binding</td>
<td style="text-align: right;">416</td>
<td style="text-align: right;">131</td>
<td style="text-align: right;">116.52</td>
<td style="text-align: left;">0.04249</td>
</tr>
<tr class="even">
<td style="text-align: left;">39</td>
<td style="text-align: left;"><a href="GO:0032555"
class="uri">GO:0032555</a></td>
<td style="text-align: left;">purine ribonucleotide binding</td>
<td style="text-align: right;">416</td>
<td style="text-align: right;">131</td>
<td style="text-align: right;">116.52</td>
<td style="text-align: left;">0.04249</td>
</tr>
<tr class="odd">
<td style="text-align: left;">40</td>
<td style="text-align: left;"><a href="GO:0003887"
class="uri">GO:0003887</a></td>
<td style="text-align: left;">DNA-directed DNA polymerase activity</td>
<td style="text-align: right;">47</td>
<td style="text-align: right;">19</td>
<td style="text-align: right;">13.17</td>
<td style="text-align: left;">0.04290</td>
</tr>
<tr class="even">
<td style="text-align: left;">41</td>
<td style="text-align: left;"><a href="GO:0008046"
class="uri">GO:0008046</a></td>
<td style="text-align: left;">axon guidance receptor activity</td>
<td style="text-align: right;">13</td>
<td style="text-align: right;">7</td>
<td style="text-align: right;">3.64</td>
<td style="text-align: left;">0.04356</td>
</tr>
<tr class="odd">
<td style="text-align: left;">42</td>
<td style="text-align: left;"><a href="GO:0017076"
class="uri">GO:0017076</a></td>
<td style="text-align: left;">purine nucleotide binding</td>
<td style="text-align: right;">418</td>
<td style="text-align: right;">131</td>
<td style="text-align: right;">117.08</td>
<td style="text-align: left;">0.04932</td>
</tr>
</tbody>
</table>

Table 6: GO-Term processes overrepresented in the set of overexpressed
DEGs.

<table>
<colgroup>
<col style="width: 3%" />
<col style="width: 11%" />
<col style="width: 45%" />
<col style="width: 10%" />
<col style="width: 12%" />
<col style="width: 9%" />
<col style="width: 8%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: left;"></th>
<th style="text-align: left;">GO.ID</th>
<th style="text-align: left;">Term</th>
<th style="text-align: right;">Annotated</th>
<th style="text-align: right;">Significant</th>
<th style="text-align: right;">Expected</th>
<th style="text-align: left;">classic</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">4</td>
<td style="text-align: left;"><a href="GO:0031344"
class="uri">GO:0031344</a></td>
<td style="text-align: left;">regulation of cell projection
organizati…</td>
<td style="text-align: right;">65</td>
<td style="text-align: right;">32</td>
<td style="text-align: right;">18.01</td>
<td style="text-align: left;">0.00014</td>
</tr>
<tr class="even">
<td style="text-align: left;">5</td>
<td style="text-align: left;"><a href="GO:0010243"
class="uri">GO:0010243</a></td>
<td style="text-align: left;">response to organonitrogen compound</td>
<td style="text-align: right;">71</td>
<td style="text-align: right;">34</td>
<td style="text-align: right;">19.68</td>
<td style="text-align: left;">0.00018</td>
</tr>
<tr class="odd">
<td style="text-align: left;">6</td>
<td style="text-align: left;"><a href="GO:0120035"
class="uri">GO:0120035</a></td>
<td style="text-align: left;">regulation of plasma membrane bounded
ce…</td>
<td style="text-align: right;">64</td>
<td style="text-align: right;">31</td>
<td style="text-align: right;">17.74</td>
<td style="text-align: left;">0.00027</td>
</tr>
<tr class="even">
<td style="text-align: left;">7</td>
<td style="text-align: left;"><a href="GO:0030595"
class="uri">GO:0030595</a></td>
<td style="text-align: left;">leukocyte chemotaxis</td>
<td style="text-align: right;">15</td>
<td style="text-align: right;">11</td>
<td style="text-align: right;">4.16</td>
<td style="text-align: left;">0.00030</td>
</tr>
<tr class="odd">
<td style="text-align: left;">8</td>
<td style="text-align: left;"><a href="GO:0006898"
class="uri">GO:0006898</a></td>
<td style="text-align: left;">receptor-mediated endocytosis</td>
<td style="text-align: right;">59</td>
<td style="text-align: right;">29</td>
<td style="text-align: right;">16.35</td>
<td style="text-align: left;">0.00031</td>
</tr>
<tr class="even">
<td style="text-align: left;">9</td>
<td style="text-align: left;"><a href="GO:0051240"
class="uri">GO:0051240</a></td>
<td style="text-align: left;">positive regulation of multicellular
org…</td>
<td style="text-align: right;">84</td>
<td style="text-align: right;">38</td>
<td style="text-align: right;">23.28</td>
<td style="text-align: left;">0.00033</td>
</tr>
<tr class="odd">
<td style="text-align: left;">10</td>
<td style="text-align: left;"><a href="GO:0006897"
class="uri">GO:0006897</a></td>
<td style="text-align: left;">endocytosis</td>
<td style="text-align: right;">111</td>
<td style="text-align: right;">47</td>
<td style="text-align: right;">30.76</td>
<td style="text-align: left;">0.00044</td>
</tr>
<tr class="even">
<td style="text-align: left;">11</td>
<td style="text-align: left;"><a href="GO:0050900"
class="uri">GO:0050900</a></td>
<td style="text-align: left;">leukocyte migration</td>
<td style="text-align: right;">39</td>
<td style="text-align: right;">21</td>
<td style="text-align: right;">10.81</td>
<td style="text-align: left;">0.00045</td>
</tr>
<tr class="odd">
<td style="text-align: left;">12</td>
<td style="text-align: left;"><a href="GO:1901698"
class="uri">GO:1901698</a></td>
<td style="text-align: left;">response to nitrogen compound</td>
<td style="text-align: right;">77</td>
<td style="text-align: right;">35</td>
<td style="text-align: right;">21.34</td>
<td style="text-align: left;">0.00051</td>
</tr>
<tr class="even">
<td style="text-align: left;">13</td>
<td style="text-align: left;"><a href="GO:0060284"
class="uri">GO:0060284</a></td>
<td style="text-align: left;">regulation of cell development</td>
<td style="text-align: right;">55</td>
<td style="text-align: right;">27</td>
<td style="text-align: right;">15.24</td>
<td style="text-align: left;">0.00051</td>
</tr>
<tr class="odd">
<td style="text-align: left;">14</td>
<td style="text-align: left;"><a href="GO:0051259"
class="uri">GO:0051259</a></td>
<td style="text-align: left;">protein complex oligomerization</td>
<td style="text-align: right;">61</td>
<td style="text-align: right;">29</td>
<td style="text-align: right;">16.90</td>
<td style="text-align: left;">0.00063</td>
</tr>
<tr class="even">
<td style="text-align: left;">15</td>
<td style="text-align: left;"><a href="GO:0044057"
class="uri">GO:0044057</a></td>
<td style="text-align: left;">regulation of system process</td>
<td style="text-align: right;">25</td>
<td style="text-align: right;">15</td>
<td style="text-align: right;">6.93</td>
<td style="text-align: left;">0.00066</td>
</tr>
<tr class="odd">
<td style="text-align: left;">16</td>
<td style="text-align: left;"><a href="GO:0021772"
class="uri">GO:0021772</a></td>
<td style="text-align: left;">olfactory bulb development</td>
<td style="text-align: right;">16</td>
<td style="text-align: right;">11</td>
<td style="text-align: right;">4.43</td>
<td style="text-align: left;">0.00071</td>
</tr>
<tr class="even">
<td style="text-align: left;">17</td>
<td style="text-align: left;"><a href="GO:0021988"
class="uri">GO:0021988</a></td>
<td style="text-align: left;">olfactory lobe development</td>
<td style="text-align: right;">16</td>
<td style="text-align: right;">11</td>
<td style="text-align: right;">4.43</td>
<td style="text-align: left;">0.00071</td>
</tr>
<tr class="odd">
<td style="text-align: left;">18</td>
<td style="text-align: left;"><a href="GO:0031346"
class="uri">GO:0031346</a></td>
<td style="text-align: left;">positive regulation of cell projection
o…</td>
<td style="text-align: right;">30</td>
<td style="text-align: right;">17</td>
<td style="text-align: right;">8.31</td>
<td style="text-align: left;">0.00073</td>
</tr>
<tr class="even">
<td style="text-align: left;">19</td>
<td style="text-align: left;"><a href="GO:0050920"
class="uri">GO:0050920</a></td>
<td style="text-align: left;">regulation of chemotaxis</td>
<td style="text-align: right;">30</td>
<td style="text-align: right;">17</td>
<td style="text-align: right;">8.31</td>
<td style="text-align: left;">0.00073</td>
</tr>
<tr class="odd">
<td style="text-align: left;">20</td>
<td style="text-align: left;"><a href="GO:0071674"
class="uri">GO:0071674</a></td>
<td style="text-align: left;">mononuclear cell migration</td>
<td style="text-align: right;">23</td>
<td style="text-align: right;">14</td>
<td style="text-align: right;">6.37</td>
<td style="text-align: left;">0.00083</td>
</tr>
<tr class="even">
<td style="text-align: left;">21</td>
<td style="text-align: left;"><a href="GO:0010975"
class="uri">GO:0010975</a></td>
<td style="text-align: left;">regulation of neuron projection
developm…</td>
<td style="text-align: right;">51</td>
<td style="text-align: right;">25</td>
<td style="text-align: right;">14.13</td>
<td style="text-align: left;">0.00084</td>
</tr>
<tr class="odd">
<td style="text-align: left;">22</td>
<td style="text-align: left;"><a href="GO:0002688"
class="uri">GO:0002688</a></td>
<td style="text-align: left;">regulation of leukocyte chemotaxis</td>
<td style="text-align: right;">10</td>
<td style="text-align: right;">8</td>
<td style="text-align: right;">2.77</td>
<td style="text-align: left;">0.00086</td>
</tr>
<tr class="even">
<td style="text-align: left;">23</td>
<td style="text-align: left;"><a href="GO:0043552"
class="uri">GO:0043552</a></td>
<td style="text-align: left;">positive regulation of
phosphatidylinosi…</td>
<td style="text-align: right;">10</td>
<td style="text-align: right;">8</td>
<td style="text-align: right;">2.77</td>
<td style="text-align: left;">0.00086</td>
</tr>
<tr class="odd">
<td style="text-align: left;">24</td>
<td style="text-align: left;"><a href="GO:0090218"
class="uri">GO:0090218</a></td>
<td style="text-align: left;">positive regulation of lipid kinase
acti…</td>
<td style="text-align: right;">10</td>
<td style="text-align: right;">8</td>
<td style="text-align: right;">2.77</td>
<td style="text-align: left;">0.00086</td>
</tr>
<tr class="even">
<td style="text-align: left;">25</td>
<td style="text-align: left;"><a href="GO:0014074"
class="uri">GO:0014074</a></td>
<td style="text-align: left;">response to purine-containing
compound</td>
<td style="text-align: right;">12</td>
<td style="text-align: right;">9</td>
<td style="text-align: right;">3.33</td>
<td style="text-align: left;">0.00087</td>
</tr>
<tr class="odd">
<td style="text-align: left;">26</td>
<td style="text-align: left;"><a href="GO:0050731"
class="uri">GO:0050731</a></td>
<td style="text-align: left;">positive regulation of
peptidyl-tyrosine…</td>
<td style="text-align: right;">12</td>
<td style="text-align: right;">9</td>
<td style="text-align: right;">3.33</td>
<td style="text-align: left;">0.00087</td>
</tr>
<tr class="even">
<td style="text-align: left;">27</td>
<td style="text-align: left;"><a href="GO:0051289"
class="uri">GO:0051289</a></td>
<td style="text-align: left;">protein homotetramerization</td>
<td style="text-align: right;">12</td>
<td style="text-align: right;">9</td>
<td style="text-align: right;">3.33</td>
<td style="text-align: left;">0.00087</td>
</tr>
<tr class="odd">
<td style="text-align: left;">28</td>
<td style="text-align: left;"><a href="GO:0030111"
class="uri">GO:0030111</a></td>
<td style="text-align: left;">regulation of Wnt signaling pathway</td>
<td style="text-align: right;">38</td>
<td style="text-align: right;">20</td>
<td style="text-align: right;">10.53</td>
<td style="text-align: left;">0.00090</td>
</tr>
<tr class="even">
<td style="text-align: left;">29</td>
<td style="text-align: left;"><a href="GO:0006935"
class="uri">GO:0006935</a></td>
<td style="text-align: left;">chemotaxis</td>
<td style="text-align: right;">100</td>
<td style="text-align: right;">42</td>
<td style="text-align: right;">27.71</td>
<td style="text-align: left;">0.00110</td>
</tr>
<tr class="odd">
<td style="text-align: left;">30</td>
<td style="text-align: left;"><a href="GO:1901652"
class="uri">GO:1901652</a></td>
<td style="text-align: left;">response to peptide</td>
<td style="text-align: right;">34</td>
<td style="text-align: right;">18</td>
<td style="text-align: right;">9.42</td>
<td style="text-align: left;">0.00149</td>
</tr>
<tr class="even">
<td style="text-align: left;">31</td>
<td style="text-align: left;"><a href="GO:1901653"
class="uri">GO:1901653</a></td>
<td style="text-align: left;">cellular response to peptide</td>
<td style="text-align: right;">29</td>
<td style="text-align: right;">16</td>
<td style="text-align: right;">8.04</td>
<td style="text-align: left;">0.00155</td>
</tr>
<tr class="odd">
<td style="text-align: left;">32</td>
<td style="text-align: left;"><a href="GO:0044257"
class="uri">GO:0044257</a></td>
<td style="text-align: left;">cellular protein catabolic process</td>
<td style="text-align: right;">81</td>
<td style="text-align: right;">35</td>
<td style="text-align: right;">22.45</td>
<td style="text-align: left;">0.00158</td>
</tr>
<tr class="even">
<td style="text-align: left;">33</td>
<td style="text-align: left;"><a href="GO:0030163"
class="uri">GO:0030163</a></td>
<td style="text-align: left;">protein catabolic process</td>
<td style="text-align: right;">93</td>
<td style="text-align: right;">39</td>
<td style="text-align: right;">25.77</td>
<td style="text-align: left;">0.00172</td>
</tr>
<tr class="odd">
<td style="text-align: left;">34</td>
<td style="text-align: left;"><a href="GO:0035567"
class="uri">GO:0035567</a></td>
<td style="text-align: left;">non-canonical Wnt signaling pathway</td>
<td style="text-align: right;">22</td>
<td style="text-align: right;">13</td>
<td style="text-align: right;">6.10</td>
<td style="text-align: left;">0.00187</td>
</tr>
<tr class="even">
<td style="text-align: left;">35</td>
<td style="text-align: left;"><a href="GO:0060326"
class="uri">GO:0060326</a></td>
<td style="text-align: left;">cell chemotaxis</td>
<td style="text-align: right;">27</td>
<td style="text-align: right;">15</td>
<td style="text-align: right;">7.48</td>
<td style="text-align: left;">0.00198</td>
</tr>
<tr class="odd">
<td style="text-align: left;">36</td>
<td style="text-align: left;"><a href="GO:0051603"
class="uri">GO:0051603</a></td>
<td style="text-align: left;">proteolysis involved in cellular
protein…</td>
<td style="text-align: right;">79</td>
<td style="text-align: right;">34</td>
<td style="text-align: right;">21.89</td>
<td style="text-align: left;">0.00200</td>
</tr>
<tr class="even">
<td style="text-align: left;">37</td>
<td style="text-align: left;"><a href="GO:0050730"
class="uri">GO:0050730</a></td>
<td style="text-align: left;">regulation of peptidyl-tyrosine
phosphor…</td>
<td style="text-align: right;">13</td>
<td style="text-align: right;">9</td>
<td style="text-align: right;">3.60</td>
<td style="text-align: left;">0.00213</td>
</tr>
<tr class="odd">
<td style="text-align: left;">38</td>
<td style="text-align: left;"><a href="GO:0060401"
class="uri">GO:0060401</a></td>
<td style="text-align: left;">cytosolic calcium ion transport</td>
<td style="text-align: right;">13</td>
<td style="text-align: right;">9</td>
<td style="text-align: right;">3.60</td>
<td style="text-align: left;">0.00213</td>
</tr>
<tr class="even">
<td style="text-align: left;">39</td>
<td style="text-align: left;"><a href="GO:0060402"
class="uri">GO:0060402</a></td>
<td style="text-align: left;">calcium ion transport into cytosol</td>
<td style="text-align: right;">13</td>
<td style="text-align: right;">9</td>
<td style="text-align: right;">3.60</td>
<td style="text-align: left;">0.00213</td>
</tr>
<tr class="odd">
<td style="text-align: left;">40</td>
<td style="text-align: left;"><a href="GO:0006887"
class="uri">GO:0006887</a></td>
<td style="text-align: left;">exocytosis</td>
<td style="text-align: right;">51</td>
<td style="text-align: right;">24</td>
<td style="text-align: right;">14.13</td>
<td style="text-align: left;">0.00219</td>
</tr>
<tr class="even">
<td style="text-align: left;">41</td>
<td style="text-align: left;"><a href="GO:0043604"
class="uri">GO:0043604</a></td>
<td style="text-align: left;">amide biosynthetic process</td>
<td style="text-align: right;">115</td>
<td style="text-align: right;">46</td>
<td style="text-align: right;">31.87</td>
<td style="text-align: left;">0.00220</td>
</tr>
<tr class="odd">
<td style="text-align: left;">42</td>
<td style="text-align: left;"><a href="GO:0042330"
class="uri">GO:0042330</a></td>
<td style="text-align: left;">taxis</td>
<td style="text-align: right;">103</td>
<td style="text-align: right;">42</td>
<td style="text-align: right;">28.54</td>
<td style="text-align: left;">0.00221</td>
</tr>
<tr class="even">
<td style="text-align: left;">43</td>
<td style="text-align: left;"><a href="GO:0043043"
class="uri">GO:0043043</a></td>
<td style="text-align: left;">peptide biosynthetic process</td>
<td style="text-align: right;">109</td>
<td style="text-align: right;">44</td>
<td style="text-align: right;">30.21</td>
<td style="text-align: left;">0.00222</td>
</tr>
<tr class="odd">
<td style="text-align: left;">44</td>
<td style="text-align: left;"><a href="GO:1905114"
class="uri">GO:1905114</a></td>
<td style="text-align: left;">cell surface receptor signaling pathway
…</td>
<td style="text-align: right;">54</td>
<td style="text-align: right;">25</td>
<td style="text-align: right;">14.96</td>
<td style="text-align: left;">0.00237</td>
</tr>
<tr class="even">
<td style="text-align: left;">45</td>
<td style="text-align: left;"><a href="GO:0002685"
class="uri">GO:0002685</a></td>
<td style="text-align: left;">regulation of leukocyte migration</td>
<td style="text-align: right;">11</td>
<td style="text-align: right;">8</td>
<td style="text-align: right;">3.05</td>
<td style="text-align: left;">0.00240</td>
</tr>
<tr class="odd">
<td style="text-align: left;">46</td>
<td style="text-align: left;"><a href="GO:0007405"
class="uri">GO:0007405</a></td>
<td style="text-align: left;">neuroblast proliferation</td>
<td style="text-align: right;">11</td>
<td style="text-align: right;">8</td>
<td style="text-align: right;">3.05</td>
<td style="text-align: left;">0.00240</td>
</tr>
<tr class="even">
<td style="text-align: left;">47</td>
<td style="text-align: left;"><a href="GO:0021889"
class="uri">GO:0021889</a></td>
<td style="text-align: left;">olfactory bulb interneuron
differentiati…</td>
<td style="text-align: right;">11</td>
<td style="text-align: right;">8</td>
<td style="text-align: right;">3.05</td>
<td style="text-align: left;">0.00240</td>
</tr>
<tr class="odd">
<td style="text-align: left;">48</td>
<td style="text-align: left;"><a href="GO:0021891"
class="uri">GO:0021891</a></td>
<td style="text-align: left;">olfactory bulb interneuron
development</td>
<td style="text-align: right;">11</td>
<td style="text-align: right;">8</td>
<td style="text-align: right;">3.05</td>
<td style="text-align: left;">0.00240</td>
</tr>
<tr class="even">
<td style="text-align: left;">49</td>
<td style="text-align: left;"><a href="GO:0043550"
class="uri">GO:0043550</a></td>
<td style="text-align: left;">regulation of lipid kinase activity</td>
<td style="text-align: right;">11</td>
<td style="text-align: right;">8</td>
<td style="text-align: right;">3.05</td>
<td style="text-align: left;">0.00240</td>
</tr>
<tr class="odd">
<td style="text-align: left;">50</td>
<td style="text-align: left;"><a href="GO:0043551"
class="uri">GO:0043551</a></td>
<td style="text-align: left;">regulation of phosphatidylinositol
3-kin…</td>
<td style="text-align: right;">11</td>
<td style="text-align: right;">8</td>
<td style="text-align: right;">3.05</td>
<td style="text-align: left;">0.00240</td>
</tr>
</tbody>
</table>

Table 7: GO-Term compartments overrepresented in the set of
overexpressed DEGs.

<table>
<colgroup>
<col style="width: 3%" />
<col style="width: 11%" />
<col style="width: 43%" />
<col style="width: 10%" />
<col style="width: 12%" />
<col style="width: 9%" />
<col style="width: 8%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: left;"></th>
<th style="text-align: left;">GO.ID</th>
<th style="text-align: left;">Term</th>
<th style="text-align: right;">Annotated</th>
<th style="text-align: right;">Significant</th>
<th style="text-align: right;">Expected</th>
<th style="text-align: left;">classic</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">4</td>
<td style="text-align: left;"><a href="GO:0031012"
class="uri">GO:0031012</a></td>
<td style="text-align: left;">extracellular matrix</td>
<td style="text-align: right;">58</td>
<td style="text-align: right;">29</td>
<td style="text-align: right;">16.28</td>
<td style="text-align: left;">0.00027</td>
</tr>
<tr class="even">
<td style="text-align: left;">5</td>
<td style="text-align: left;"><a href="GO:0005604"
class="uri">GO:0005604</a></td>
<td style="text-align: left;">basement membrane</td>
<td style="text-align: right;">41</td>
<td style="text-align: right;">22</td>
<td style="text-align: right;">11.51</td>
<td style="text-align: left;">0.00043</td>
</tr>
<tr class="odd">
<td style="text-align: left;">6</td>
<td style="text-align: left;"><a href="GO:0005840"
class="uri">GO:0005840</a></td>
<td style="text-align: left;">ribosome</td>
<td style="text-align: right;">54</td>
<td style="text-align: right;">27</td>
<td style="text-align: right;">15.16</td>
<td style="text-align: left;">0.00044</td>
</tr>
<tr class="even">
<td style="text-align: left;">7</td>
<td style="text-align: left;"><a href="GO:0062023"
class="uri">GO:0062023</a></td>
<td style="text-align: left;">collagen-containing extracellular
matrix</td>
<td style="text-align: right;">47</td>
<td style="text-align: right;">24</td>
<td style="text-align: right;">13.19</td>
<td style="text-align: left;">0.00062</td>
</tr>
<tr class="odd">
<td style="text-align: left;">8</td>
<td style="text-align: left;"><a href="GO:0098552"
class="uri">GO:0098552</a></td>
<td style="text-align: left;">side of membrane</td>
<td style="text-align: right;">51</td>
<td style="text-align: right;">25</td>
<td style="text-align: right;">14.32</td>
<td style="text-align: left;">0.00104</td>
</tr>
<tr class="even">
<td style="text-align: left;">9</td>
<td style="text-align: left;"><a href="GO:0019897"
class="uri">GO:0019897</a></td>
<td style="text-align: left;">extrinsic component of plasma
membrane</td>
<td style="text-align: right;">28</td>
<td style="text-align: right;">16</td>
<td style="text-align: right;">7.86</td>
<td style="text-align: left;">0.00108</td>
</tr>
<tr class="odd">
<td style="text-align: left;">10</td>
<td style="text-align: left;"><a href="GO:0005618"
class="uri">GO:0005618</a></td>
<td style="text-align: left;">cell wall</td>
<td style="text-align: right;">21</td>
<td style="text-align: right;">13</td>
<td style="text-align: right;">5.89</td>
<td style="text-align: left;">0.00117</td>
</tr>
<tr class="even">
<td style="text-align: left;">11</td>
<td style="text-align: left;"><a href="GO:0071944"
class="uri">GO:0071944</a></td>
<td style="text-align: left;">cell periphery</td>
<td style="text-align: right;">523</td>
<td style="text-align: right;">174</td>
<td style="text-align: right;">146.80</td>
<td style="text-align: left;">0.00118</td>
</tr>
<tr class="odd">
<td style="text-align: left;">12</td>
<td style="text-align: left;"><a href="GO:0042588"
class="uri">GO:0042588</a></td>
<td style="text-align: left;">zymogen granule</td>
<td style="text-align: right;">19</td>
<td style="text-align: right;">12</td>
<td style="text-align: right;">5.33</td>
<td style="text-align: left;">0.00142</td>
</tr>
<tr class="even">
<td style="text-align: left;">13</td>
<td style="text-align: left;"><a href="GO:0042589"
class="uri">GO:0042589</a></td>
<td style="text-align: left;">zymogen granule membrane</td>
<td style="text-align: right;">19</td>
<td style="text-align: right;">12</td>
<td style="text-align: right;">5.33</td>
<td style="text-align: left;">0.00142</td>
</tr>
<tr class="odd">
<td style="text-align: left;">14</td>
<td style="text-align: left;"><a href="GO:0005581"
class="uri">GO:0005581</a></td>
<td style="text-align: left;">collagen trimer</td>
<td style="text-align: right;">21</td>
<td style="text-align: right;">12</td>
<td style="text-align: right;">5.89</td>
<td style="text-align: left;">0.00464</td>
</tr>
<tr class="even">
<td style="text-align: left;">15</td>
<td style="text-align: left;"><a href="GO:0110165"
class="uri">GO:0110165</a></td>
<td style="text-align: left;">cellular anatomical entity</td>
<td style="text-align: right;">1771</td>
<td style="text-align: right;">507</td>
<td style="text-align: right;">497.11</td>
<td style="text-align: left;">0.00569</td>
</tr>
<tr class="odd">
<td style="text-align: left;">16</td>
<td style="text-align: left;"><a href="GO:0022627"
class="uri">GO:0022627</a></td>
<td style="text-align: left;">cytosolic small ribosomal subunit</td>
<td style="text-align: right;">12</td>
<td style="text-align: right;">8</td>
<td style="text-align: right;">3.37</td>
<td style="text-align: left;">0.00595</td>
</tr>
<tr class="even">
<td style="text-align: left;">17</td>
<td style="text-align: left;"><a href="GO:0043083"
class="uri">GO:0043083</a></td>
<td style="text-align: left;">synaptic cleft</td>
<td style="text-align: right;">12</td>
<td style="text-align: right;">8</td>
<td style="text-align: right;">3.37</td>
<td style="text-align: left;">0.00595</td>
</tr>
<tr class="odd">
<td style="text-align: left;">18</td>
<td style="text-align: left;"><a href="GO:0019867"
class="uri">GO:0019867</a></td>
<td style="text-align: left;">outer membrane</td>
<td style="text-align: right;">24</td>
<td style="text-align: right;">13</td>
<td style="text-align: right;">6.74</td>
<td style="text-align: left;">0.00599</td>
</tr>
<tr class="even">
<td style="text-align: left;">19</td>
<td style="text-align: left;"><a href="GO:0005886"
class="uri">GO:0005886</a></td>
<td style="text-align: left;">plasma membrane</td>
<td style="text-align: right;">462</td>
<td style="text-align: right;">151</td>
<td style="text-align: right;">129.68</td>
<td style="text-align: left;">0.00678</td>
</tr>
<tr class="odd">
<td style="text-align: left;">20</td>
<td style="text-align: left;"><a href="GO:0043195"
class="uri">GO:0043195</a></td>
<td style="text-align: left;">terminal bouton</td>
<td style="text-align: right;">22</td>
<td style="text-align: right;">12</td>
<td style="text-align: right;">6.18</td>
<td style="text-align: left;">0.00766</td>
</tr>
<tr class="even">
<td style="text-align: left;">21</td>
<td style="text-align: left;"><a href="GO:0045335"
class="uri">GO:0045335</a></td>
<td style="text-align: left;">phagocytic vesicle</td>
<td style="text-align: right;">22</td>
<td style="text-align: right;">12</td>
<td style="text-align: right;">6.18</td>
<td style="text-align: left;">0.00766</td>
</tr>
<tr class="odd">
<td style="text-align: left;">22</td>
<td style="text-align: left;"><a href="GO:0044391"
class="uri">GO:0044391</a></td>
<td style="text-align: left;">ribosomal subunit</td>
<td style="text-align: right;">38</td>
<td style="text-align: right;">18</td>
<td style="text-align: right;">10.67</td>
<td style="text-align: left;">0.00822</td>
</tr>
<tr class="even">
<td style="text-align: left;">23</td>
<td style="text-align: left;"><a href="GO:0015935"
class="uri">GO:0015935</a></td>
<td style="text-align: left;">small ribosomal subunit</td>
<td style="text-align: right;">15</td>
<td style="text-align: right;">9</td>
<td style="text-align: right;">4.21</td>
<td style="text-align: left;">0.00935</td>
</tr>
<tr class="odd">
<td style="text-align: left;">24</td>
<td style="text-align: left;"><a href="GO:0098562"
class="uri">GO:0098562</a></td>
<td style="text-align: left;">cytoplasmic side of membrane</td>
<td style="text-align: right;">39</td>
<td style="text-align: right;">18</td>
<td style="text-align: right;">10.95</td>
<td style="text-align: left;">0.01138</td>
</tr>
<tr class="even">
<td style="text-align: left;">25</td>
<td style="text-align: left;"><a href="GO:0030667"
class="uri">GO:0030667</a></td>
<td style="text-align: left;">secretory granule membrane</td>
<td style="text-align: right;">26</td>
<td style="text-align: right;">13</td>
<td style="text-align: right;">7.30</td>
<td style="text-align: left;">0.01400</td>
</tr>
<tr class="odd">
<td style="text-align: left;">26</td>
<td style="text-align: left;"><a href="GO:0022626"
class="uri">GO:0022626</a></td>
<td style="text-align: left;">cytosolic ribosome</td>
<td style="text-align: right;">29</td>
<td style="text-align: right;">14</td>
<td style="text-align: right;">8.14</td>
<td style="text-align: left;">0.01573</td>
</tr>
<tr class="even">
<td style="text-align: left;">27</td>
<td style="text-align: left;"><a href="GO:0043679"
class="uri">GO:0043679</a></td>
<td style="text-align: left;">axon terminus</td>
<td style="text-align: right;">24</td>
<td style="text-align: right;">12</td>
<td style="text-align: right;">6.74</td>
<td style="text-align: left;">0.01805</td>
</tr>
<tr class="odd">
<td style="text-align: left;">28</td>
<td style="text-align: left;"><a href="GO:0044306"
class="uri">GO:0044306</a></td>
<td style="text-align: left;">neuron projection terminus</td>
<td style="text-align: right;">24</td>
<td style="text-align: right;">12</td>
<td style="text-align: right;">6.74</td>
<td style="text-align: left;">0.01805</td>
</tr>
<tr class="even">
<td style="text-align: left;">29</td>
<td style="text-align: left;"><a href="GO:0030424"
class="uri">GO:0030424</a></td>
<td style="text-align: left;">axon</td>
<td style="text-align: right;">83</td>
<td style="text-align: right;">32</td>
<td style="text-align: right;">23.30</td>
<td style="text-align: left;">0.02238</td>
</tr>
<tr class="odd">
<td style="text-align: left;">30</td>
<td style="text-align: left;"><a href="GO:0099503"
class="uri">GO:0099503</a></td>
<td style="text-align: left;">secretory vesicle</td>
<td style="text-align: right;">53</td>
<td style="text-align: right;">22</td>
<td style="text-align: right;">14.88</td>
<td style="text-align: left;">0.02269</td>
</tr>
<tr class="even">
<td style="text-align: left;">31</td>
<td style="text-align: left;"><a href="GO:0009986"
class="uri">GO:0009986</a></td>
<td style="text-align: left;">cell surface</td>
<td style="text-align: right;">65</td>
<td style="text-align: right;">26</td>
<td style="text-align: right;">18.24</td>
<td style="text-align: left;">0.02324</td>
</tr>
<tr class="odd">
<td style="text-align: left;">32</td>
<td style="text-align: left;"><a href="GO:0098793"
class="uri">GO:0098793</a></td>
<td style="text-align: left;">presynapse</td>
<td style="text-align: right;">33</td>
<td style="text-align: right;">15</td>
<td style="text-align: right;">9.26</td>
<td style="text-align: left;">0.02365</td>
</tr>
<tr class="even">
<td style="text-align: left;">33</td>
<td style="text-align: left;"><a href="GO:0005615"
class="uri">GO:0005615</a></td>
<td style="text-align: left;">extracellular space</td>
<td style="text-align: right;">261</td>
<td style="text-align: right;">87</td>
<td style="text-align: right;">73.26</td>
<td style="text-align: left;">0.02571</td>
</tr>
<tr class="odd">
<td style="text-align: left;">34</td>
<td style="text-align: left;"><a href="GO:0009897"
class="uri">GO:0009897</a></td>
<td style="text-align: left;">external side of plasma membrane</td>
<td style="text-align: right;">12</td>
<td style="text-align: right;">7</td>
<td style="text-align: right;">3.37</td>
<td style="text-align: left;">0.02663</td>
</tr>
<tr class="even">
<td style="text-align: left;">35</td>
<td style="text-align: left;"><a href="GO:0099081"
class="uri">GO:0099081</a></td>
<td style="text-align: left;">supramolecular polymer</td>
<td style="text-align: right;">94</td>
<td style="text-align: right;">35</td>
<td style="text-align: right;">26.39</td>
<td style="text-align: left;">0.03020</td>
</tr>
<tr class="odd">
<td style="text-align: left;">36</td>
<td style="text-align: left;"><a href="GO:0005788"
class="uri">GO:0005788</a></td>
<td style="text-align: left;">endoplasmic reticulum lumen</td>
<td style="text-align: right;">20</td>
<td style="text-align: right;">10</td>
<td style="text-align: right;">5.61</td>
<td style="text-align: left;">0.03024</td>
</tr>
<tr class="even">
<td style="text-align: left;">37</td>
<td style="text-align: left;"><a href="GO:0030139"
class="uri">GO:0030139</a></td>
<td style="text-align: left;">endocytic vesicle</td>
<td style="text-align: right;">34</td>
<td style="text-align: right;">15</td>
<td style="text-align: right;">9.54</td>
<td style="text-align: left;">0.03170</td>
</tr>
<tr class="odd">
<td style="text-align: left;">38</td>
<td style="text-align: left;"><a href="GO:0099512"
class="uri">GO:0099512</a></td>
<td style="text-align: left;">supramolecular fiber</td>
<td style="text-align: right;">93</td>
<td style="text-align: right;">34</td>
<td style="text-align: right;">26.10</td>
<td style="text-align: left;">0.04233</td>
</tr>
<tr class="even">
<td style="text-align: left;">39</td>
<td style="text-align: left;"><a href="GO:0030141"
class="uri">GO:0030141</a></td>
<td style="text-align: left;">secretory granule</td>
<td style="text-align: right;">44</td>
<td style="text-align: right;">18</td>
<td style="text-align: right;">12.35</td>
<td style="text-align: left;">0.04355</td>
</tr>
<tr class="odd">
<td style="text-align: left;">40</td>
<td style="text-align: left;"><a href="GO:0009279"
class="uri">GO:0009279</a></td>
<td style="text-align: left;">cell outer membrane</td>
<td style="text-align: right;">13</td>
<td style="text-align: right;">7</td>
<td style="text-align: right;">3.65</td>
<td style="text-align: left;">0.04403</td>
</tr>
<tr class="even">
<td style="text-align: left;">41</td>
<td style="text-align: left;"><a href="GO:0045202"
class="uri">GO:0045202</a></td>
<td style="text-align: left;">synapse</td>
<td style="text-align: right;">81</td>
<td style="text-align: right;">30</td>
<td style="text-align: right;">22.74</td>
<td style="text-align: left;">0.04614</td>
</tr>
<tr class="odd">
<td style="text-align: left;">42</td>
<td style="text-align: left;"><a href="GO:0031982"
class="uri">GO:0031982</a></td>
<td style="text-align: left;">vesicle</td>
<td style="text-align: right;">362</td>
<td style="text-align: right;">115</td>
<td style="text-align: right;">101.61</td>
<td style="text-align: left;">0.04731</td>
</tr>
</tbody>
</table>

Table 8: GO-Term functions overrepresented in the set of underexpressed
DEGs.

<table>
<colgroup>
<col style="width: 3%" />
<col style="width: 11%" />
<col style="width: 45%" />
<col style="width: 10%" />
<col style="width: 12%" />
<col style="width: 9%" />
<col style="width: 8%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: left;"></th>
<th style="text-align: left;">GO.ID</th>
<th style="text-align: left;">Term</th>
<th style="text-align: right;">Annotated</th>
<th style="text-align: right;">Significant</th>
<th style="text-align: right;">Expected</th>
<th style="text-align: left;">classic</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">22</td>
<td style="text-align: left;"><a href="GO:0030246"
class="uri">GO:0030246</a></td>
<td style="text-align: left;">carbohydrate binding</td>
<td style="text-align: right;">120</td>
<td style="text-align: right;">45</td>
<td style="text-align: right;">26.82</td>
<td style="text-align: left;">0.00011</td>
</tr>
<tr class="even">
<td style="text-align: left;">23</td>
<td style="text-align: left;"><a href="GO:0016818"
class="uri">GO:0016818</a></td>
<td style="text-align: left;">hydrolase activity, acting on acid
anhyd…</td>
<td style="text-align: right;">318</td>
<td style="text-align: right;">99</td>
<td style="text-align: right;">71.08</td>
<td style="text-align: left;">0.00013</td>
</tr>
<tr class="odd">
<td style="text-align: left;">24</td>
<td style="text-align: left;"><a href="GO:0016817"
class="uri">GO:0016817</a></td>
<td style="text-align: left;">hydrolase activity, acting on acid
anhyd…</td>
<td style="text-align: right;">322</td>
<td style="text-align: right;">99</td>
<td style="text-align: right;">71.98</td>
<td style="text-align: left;">0.00021</td>
</tr>
<tr class="even">
<td style="text-align: left;">25</td>
<td style="text-align: left;"><a href="GO:0038023"
class="uri">GO:0038023</a></td>
<td style="text-align: left;">signaling receptor activity</td>
<td style="text-align: right;">302</td>
<td style="text-align: right;">93</td>
<td style="text-align: right;">67.51</td>
<td style="text-align: left;">0.00031</td>
</tr>
<tr class="odd">
<td style="text-align: left;">26</td>
<td style="text-align: left;"><a href="GO:0060089"
class="uri">GO:0060089</a></td>
<td style="text-align: left;">molecular transducer activity</td>
<td style="text-align: right;">302</td>
<td style="text-align: right;">93</td>
<td style="text-align: right;">67.51</td>
<td style="text-align: left;">0.00031</td>
</tr>
<tr class="even">
<td style="text-align: left;">27</td>
<td style="text-align: left;"><a href="GO:0004497"
class="uri">GO:0004497</a></td>
<td style="text-align: left;">monooxygenase activity</td>
<td style="text-align: right;">59</td>
<td style="text-align: right;">25</td>
<td style="text-align: right;">13.19</td>
<td style="text-align: left;">0.00044</td>
</tr>
<tr class="odd">
<td style="text-align: left;">28</td>
<td style="text-align: left;"><a href="GO:0005220"
class="uri">GO:0005220</a></td>
<td style="text-align: left;">inositol 1,4,5-trisphosphate-sensitive
c…</td>
<td style="text-align: right;">11</td>
<td style="text-align: right;">8</td>
<td style="text-align: right;">2.46</td>
<td style="text-align: left;">0.00052</td>
</tr>
<tr class="even">
<td style="text-align: left;">29</td>
<td style="text-align: left;"><a href="GO:0016705"
class="uri">GO:0016705</a></td>
<td style="text-align: left;">oxidoreductase activity, acting on
paire…</td>
<td style="text-align: right;">81</td>
<td style="text-align: right;">31</td>
<td style="text-align: right;">18.11</td>
<td style="text-align: left;">0.00082</td>
</tr>
<tr class="odd">
<td style="text-align: left;">30</td>
<td style="text-align: left;"><a href="GO:0015085"
class="uri">GO:0015085</a></td>
<td style="text-align: left;">calcium ion transmembrane transporter
ac…</td>
<td style="text-align: right;">39</td>
<td style="text-align: right;">18</td>
<td style="text-align: right;">8.72</td>
<td style="text-align: left;">0.00083</td>
</tr>
<tr class="even">
<td style="text-align: left;">31</td>
<td style="text-align: left;"><a href="GO:0015491"
class="uri">GO:0015491</a></td>
<td style="text-align: left;">cation:cation antiporter activity</td>
<td style="text-align: right;">14</td>
<td style="text-align: right;">9</td>
<td style="text-align: right;">3.13</td>
<td style="text-align: left;">0.00090</td>
</tr>
<tr class="odd">
<td style="text-align: left;">32</td>
<td style="text-align: left;"><a href="GO:0016646"
class="uri">GO:0016646</a></td>
<td style="text-align: left;">oxidoreductase activity, acting on the
C…</td>
<td style="text-align: right;">14</td>
<td style="text-align: right;">9</td>
<td style="text-align: right;">3.13</td>
<td style="text-align: left;">0.00090</td>
</tr>
<tr class="even">
<td style="text-align: left;">33</td>
<td style="text-align: left;"><a href="GO:0004888"
class="uri">GO:0004888</a></td>
<td style="text-align: left;">transmembrane signaling receptor
activit…</td>
<td style="text-align: right;">290</td>
<td style="text-align: right;">87</td>
<td style="text-align: right;">64.82</td>
<td style="text-align: left;">0.00119</td>
</tr>
<tr class="odd">
<td style="text-align: left;">34</td>
<td style="text-align: left;"><a href="GO:0061134"
class="uri">GO:0061134</a></td>
<td style="text-align: left;">peptidase regulator activity</td>
<td style="text-align: right;">56</td>
<td style="text-align: right;">23</td>
<td style="text-align: right;">12.52</td>
<td style="text-align: left;">0.00125</td>
</tr>
<tr class="even">
<td style="text-align: left;">35</td>
<td style="text-align: left;"><a href="GO:0005432"
class="uri">GO:0005432</a></td>
<td style="text-align: left;">calcium:sodium antiporter activity</td>
<td style="text-align: right;">12</td>
<td style="text-align: right;">8</td>
<td style="text-align: right;">2.68</td>
<td style="text-align: left;">0.00126</td>
</tr>
<tr class="odd">
<td style="text-align: left;">36</td>
<td style="text-align: left;"><a href="GO:0015368"
class="uri">GO:0015368</a></td>
<td style="text-align: left;">calcium:cation antiporter activity</td>
<td style="text-align: right;">12</td>
<td style="text-align: right;">8</td>
<td style="text-align: right;">2.68</td>
<td style="text-align: left;">0.00126</td>
</tr>
<tr class="even">
<td style="text-align: left;">37</td>
<td style="text-align: left;"><a href="GO:0016491"
class="uri">GO:0016491</a></td>
<td style="text-align: left;">oxidoreductase activity</td>
<td style="text-align: right;">433</td>
<td style="text-align: right;">123</td>
<td style="text-align: right;">96.79</td>
<td style="text-align: left;">0.00134</td>
</tr>
<tr class="odd">
<td style="text-align: left;">38</td>
<td style="text-align: left;"><a href="GO:0008574"
class="uri">GO:0008574</a></td>
<td style="text-align: left;">plus-end-directed microtubule motor
acti…</td>
<td style="text-align: right;">10</td>
<td style="text-align: right;">7</td>
<td style="text-align: right;">2.24</td>
<td style="text-align: left;">0.00173</td>
</tr>
<tr class="even">
<td style="text-align: left;">39</td>
<td style="text-align: left;"><a href="GO:0015081"
class="uri">GO:0015081</a></td>
<td style="text-align: left;">sodium ion transmembrane transporter
act…</td>
<td style="text-align: right;">24</td>
<td style="text-align: right;">12</td>
<td style="text-align: right;">5.36</td>
<td style="text-align: left;">0.00266</td>
</tr>
<tr class="odd">
<td style="text-align: left;">40</td>
<td style="text-align: left;"><a href="GO:0016879"
class="uri">GO:0016879</a></td>
<td style="text-align: left;">ligase activity, forming
carbon-nitrogen…</td>
<td style="text-align: right;">27</td>
<td style="text-align: right;">13</td>
<td style="text-align: right;">6.04</td>
<td style="text-align: left;">0.00273</td>
</tr>
<tr class="even">
<td style="text-align: left;">41</td>
<td style="text-align: left;"><a href="GO:0005516"
class="uri">GO:0005516</a></td>
<td style="text-align: left;">calmodulin binding</td>
<td style="text-align: right;">98</td>
<td style="text-align: right;">34</td>
<td style="text-align: right;">21.91</td>
<td style="text-align: left;">0.00333</td>
</tr>
<tr class="odd">
<td style="text-align: left;">42</td>
<td style="text-align: left;"><a href="GO:0008017"
class="uri">GO:0008017</a></td>
<td style="text-align: left;">microtubule binding</td>
<td style="text-align: right;">93</td>
<td style="text-align: right;">32</td>
<td style="text-align: right;">20.79</td>
<td style="text-align: left;">0.00499</td>
</tr>
<tr class="even">
<td style="text-align: left;">43</td>
<td style="text-align: left;"><a href="GO:0015631"
class="uri">GO:0015631</a></td>
<td style="text-align: left;">tubulin binding</td>
<td style="text-align: right;">126</td>
<td style="text-align: right;">41</td>
<td style="text-align: right;">28.17</td>
<td style="text-align: left;">0.00509</td>
</tr>
<tr class="odd">
<td style="text-align: left;">44</td>
<td style="text-align: left;"><a href="GO:0005506"
class="uri">GO:0005506</a></td>
<td style="text-align: left;">iron ion binding</td>
<td style="text-align: right;">76</td>
<td style="text-align: right;">27</td>
<td style="text-align: right;">16.99</td>
<td style="text-align: left;">0.00585</td>
</tr>
<tr class="even">
<td style="text-align: left;">45</td>
<td style="text-align: left;"><a href="GO:0016776"
class="uri">GO:0016776</a></td>
<td style="text-align: left;">phosphotransferase activity, phosphate
g…</td>
<td style="text-align: right;">20</td>
<td style="text-align: right;">10</td>
<td style="text-align: right;">4.47</td>
<td style="text-align: left;">0.00602</td>
</tr>
<tr class="odd">
<td style="text-align: left;">46</td>
<td style="text-align: left;"><a href="GO:0004725"
class="uri">GO:0004725</a></td>
<td style="text-align: left;">protein tyrosine phosphatase activity</td>
<td style="text-align: right;">70</td>
<td style="text-align: right;">25</td>
<td style="text-align: right;">15.65</td>
<td style="text-align: left;">0.00727</td>
</tr>
<tr class="even">
<td style="text-align: left;">47</td>
<td style="text-align: left;"><a href="GO:0051015"
class="uri">GO:0051015</a></td>
<td style="text-align: left;">actin filament binding</td>
<td style="text-align: right;">68</td>
<td style="text-align: right;">24</td>
<td style="text-align: right;">15.20</td>
<td style="text-align: left;">0.00992</td>
</tr>
<tr class="odd">
<td style="text-align: left;">48</td>
<td style="text-align: left;"><a href="GO:0000981"
class="uri">GO:0000981</a></td>
<td style="text-align: left;">DNA-binding transcription factor
activit…</td>
<td style="text-align: right;">25</td>
<td style="text-align: right;">11</td>
<td style="text-align: right;">5.59</td>
<td style="text-align: left;">0.01298</td>
</tr>
<tr class="even">
<td style="text-align: left;">49</td>
<td style="text-align: left;"><a href="GO:0043394"
class="uri">GO:0043394</a></td>
<td style="text-align: left;">proteoglycan binding</td>
<td style="text-align: right;">19</td>
<td style="text-align: right;">9</td>
<td style="text-align: right;">4.25</td>
<td style="text-align: left;">0.01393</td>
</tr>
<tr class="odd">
<td style="text-align: left;">50</td>
<td style="text-align: left;"><a href="GO:0042626"
class="uri">GO:0042626</a></td>
<td style="text-align: left;">ATPase-coupled transmembrane
transporter…</td>
<td style="text-align: right;">77</td>
<td style="text-align: right;">26</td>
<td style="text-align: right;">17.21</td>
<td style="text-align: left;">0.01395</td>
</tr>
</tbody>
</table>

Table 9: GO-Term processes overrepresented in the set of underexpressed
DEGs.

<table>
<colgroup>
<col style="width: 14%" />
<col style="width: 35%" />
<col style="width: 12%" />
<col style="width: 15%" />
<col style="width: 11%" />
<col style="width: 10%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: left;">GO.ID</th>
<th style="text-align: left;">Term</th>
<th style="text-align: right;">Annotated</th>
<th style="text-align: right;">Significant</th>
<th style="text-align: right;">Expected</th>
<th style="text-align: left;">classic</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;"><a href="GO:0007018"
class="uri">GO:0007018</a></td>
<td style="text-align: left;">microtubule-based movement</td>
<td style="text-align: right;">178</td>
<td style="text-align: right;">117</td>
<td style="text-align: right;">39.70</td>
<td style="text-align: left;">&lt; 1e-30</td>
</tr>
<tr class="even">
<td style="text-align: left;"><a href="GO:0007017"
class="uri">GO:0007017</a></td>
<td style="text-align: left;">microtubule-based process</td>
<td style="text-align: right;">352</td>
<td style="text-align: right;">179</td>
<td style="text-align: right;">78.52</td>
<td style="text-align: left;">&lt; 1e-30</td>
</tr>
</tbody>
</table>

Table 10: GO-Term compartments overrepresented in the set of
underexpressed DEGs.

<table>
<colgroup>
<col style="width: 3%" />
<col style="width: 11%" />
<col style="width: 43%" />
<col style="width: 10%" />
<col style="width: 12%" />
<col style="width: 9%" />
<col style="width: 8%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: left;"></th>
<th style="text-align: left;">GO.ID</th>
<th style="text-align: left;">Term</th>
<th style="text-align: right;">Annotated</th>
<th style="text-align: right;">Significant</th>
<th style="text-align: right;">Expected</th>
<th style="text-align: left;">classic</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">1</td>
<td style="text-align: left;"><a href="GO:0005929"
class="uri">GO:0005929</a></td>
<td style="text-align: left;">cilium</td>
<td style="text-align: right;">309</td>
<td style="text-align: right;">191</td>
<td style="text-align: right;">70.16</td>
<td style="text-align: left;">&lt; 1e-30</td>
</tr>
<tr class="even">
<td style="text-align: left;">2</td>
<td style="text-align: left;"><a href="GO:0005930"
class="uri">GO:0005930</a></td>
<td style="text-align: left;">axoneme</td>
<td style="text-align: right;">97</td>
<td style="text-align: right;">78</td>
<td style="text-align: right;">22.02</td>
<td style="text-align: left;">&lt; 1e-30</td>
</tr>
<tr class="odd">
<td style="text-align: left;">3</td>
<td style="text-align: left;"><a href="GO:0097014"
class="uri">GO:0097014</a></td>
<td style="text-align: left;">ciliary plasm</td>
<td style="text-align: right;">97</td>
<td style="text-align: right;">78</td>
<td style="text-align: right;">22.02</td>
<td style="text-align: left;">&lt; 1e-30</td>
</tr>
<tr class="even">
<td style="text-align: left;">39</td>
<td style="text-align: left;"><a href="GO:0000922"
class="uri">GO:0000922</a></td>
<td style="text-align: left;">spindle pole</td>
<td style="text-align: right;">63</td>
<td style="text-align: right;">28</td>
<td style="text-align: right;">14.30</td>
<td style="text-align: left;">0.00010</td>
</tr>
<tr class="odd">
<td style="text-align: left;">40</td>
<td style="text-align: left;"><a href="GO:0000795"
class="uri">GO:0000795</a></td>
<td style="text-align: left;">synaptonemal complex</td>
<td style="text-align: right;">14</td>
<td style="text-align: right;">10</td>
<td style="text-align: right;">3.18</td>
<td style="text-align: left;">0.00014</td>
</tr>
<tr class="even">
<td style="text-align: left;">41</td>
<td style="text-align: left;"><a href="GO:0099086"
class="uri">GO:0099086</a></td>
<td style="text-align: left;">synaptonemal structure</td>
<td style="text-align: right;">14</td>
<td style="text-align: right;">10</td>
<td style="text-align: right;">3.18</td>
<td style="text-align: left;">0.00014</td>
</tr>
<tr class="odd">
<td style="text-align: left;">42</td>
<td style="text-align: left;"><a href="GO:0030992"
class="uri">GO:0030992</a></td>
<td style="text-align: left;">intraciliary transport particle B</td>
<td style="text-align: right;">12</td>
<td style="text-align: right;">9</td>
<td style="text-align: right;">2.72</td>
<td style="text-align: left;">0.00018</td>
</tr>
<tr class="even">
<td style="text-align: left;">43</td>
<td style="text-align: left;"><a href="GO:0097542"
class="uri">GO:0097542</a></td>
<td style="text-align: left;">ciliary tip</td>
<td style="text-align: right;">19</td>
<td style="text-align: right;">12</td>
<td style="text-align: right;">4.31</td>
<td style="text-align: left;">0.00018</td>
</tr>
<tr class="odd">
<td style="text-align: left;">44</td>
<td style="text-align: left;"><a href="GO:0097730"
class="uri">GO:0097730</a></td>
<td style="text-align: left;">non-motile cilium</td>
<td style="text-align: right;">53</td>
<td style="text-align: right;">24</td>
<td style="text-align: right;">12.03</td>
<td style="text-align: left;">0.00022</td>
</tr>
<tr class="even">
<td style="text-align: left;">45</td>
<td style="text-align: left;"><a href="GO:0097731"
class="uri">GO:0097731</a></td>
<td style="text-align: left;">9+0 non-motile cilium</td>
<td style="text-align: right;">50</td>
<td style="text-align: right;">23</td>
<td style="text-align: right;">11.35</td>
<td style="text-align: left;">0.00022</td>
</tr>
<tr class="odd">
<td style="text-align: left;">46</td>
<td style="text-align: left;"><a href="GO:0097733"
class="uri">GO:0097733</a></td>
<td style="text-align: left;">photoreceptor cell cilium</td>
<td style="text-align: right;">50</td>
<td style="text-align: right;">23</td>
<td style="text-align: right;">11.35</td>
<td style="text-align: left;">0.00022</td>
</tr>
<tr class="even">
<td style="text-align: left;">47</td>
<td style="text-align: left;"><a href="GO:0032421"
class="uri">GO:0032421</a></td>
<td style="text-align: left;">stereocilium bundle</td>
<td style="text-align: right;">36</td>
<td style="text-align: right;">18</td>
<td style="text-align: right;">8.17</td>
<td style="text-align: left;">0.00030</td>
</tr>
<tr class="odd">
<td style="text-align: left;">48</td>
<td style="text-align: left;"><a href="GO:0098862"
class="uri">GO:0098862</a></td>
<td style="text-align: left;">cluster of actin-based cell
projections</td>
<td style="text-align: right;">70</td>
<td style="text-align: right;">29</td>
<td style="text-align: right;">15.89</td>
<td style="text-align: left;">0.00034</td>
</tr>
<tr class="even">
<td style="text-align: left;">49</td>
<td style="text-align: left;"><a href="GO:0042599"
class="uri">GO:0042599</a></td>
<td style="text-align: left;">lamellar body</td>
<td style="text-align: right;">11</td>
<td style="text-align: right;">8</td>
<td style="text-align: right;">2.50</td>
<td style="text-align: left;">0.00059</td>
</tr>
<tr class="odd">
<td style="text-align: left;">50</td>
<td style="text-align: left;"><a href="GO:0042383"
class="uri">GO:0042383</a></td>
<td style="text-align: left;">sarcolemma</td>
<td style="text-align: right;">56</td>
<td style="text-align: right;">24</td>
<td style="text-align: right;">12.71</td>
<td style="text-align: left;">0.00060</td>
</tr>
</tbody>
</table>

## Pfam domains

Over

<table>
<colgroup>
<col style="width: 4%" />
<col style="width: 18%" />
<col style="width: 17%" />
<col style="width: 24%" />
<col style="width: 12%" />
<col style="width: 22%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: left;"></th>
<th style="text-align: left;">Domain</th>
<th style="text-align: right;">Count_in_DEGs</th>
<th style="text-align: right;">Count_in_Background</th>
<th style="text-align: right;">p_value</th>
<th style="text-align: right;">Corrected_p_value</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">3</td>
<td style="text-align: left;">Peptidase_C1</td>
<td style="text-align: right;">18</td>
<td style="text-align: right;">5</td>
<td style="text-align: right;">0.0000000</td>
<td style="text-align: right;">0.0000000</td>
</tr>
<tr class="even">
<td style="text-align: left;">24</td>
<td style="text-align: left;">DDE_1</td>
<td style="text-align: right;">10</td>
<td style="text-align: right;">7</td>
<td style="text-align: right;">0.0000000</td>
<td style="text-align: right;">0.0000000</td>
</tr>
<tr class="odd">
<td style="text-align: left;">26</td>
<td style="text-align: left;">Fz</td>
<td style="text-align: right;">3</td>
<td style="text-align: right;">3</td>
<td style="text-align: right;">0.0000000</td>
<td style="text-align: right;">0.0000000</td>
</tr>
<tr class="even">
<td style="text-align: left;">42</td>
<td style="text-align: left;">UCH</td>
<td style="text-align: right;">7</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0.0000000</td>
<td style="text-align: right;">0.0000000</td>
</tr>
<tr class="odd">
<td style="text-align: left;">55</td>
<td style="text-align: left;">Fibrinogen_C</td>
<td style="text-align: right;">7</td>
<td style="text-align: right;">5</td>
<td style="text-align: right;">0.0000000</td>
<td style="text-align: right;">0.0000000</td>
</tr>
<tr class="even">
<td style="text-align: left;">61</td>
<td style="text-align: left;">Tubulin</td>
<td style="text-align: right;">12</td>
<td style="text-align: right;">6</td>
<td style="text-align: right;">0.0000000</td>
<td style="text-align: right;">0.0000000</td>
</tr>
<tr class="odd">
<td style="text-align: left;">64</td>
<td style="text-align: left;">GTP_EFTU_D3</td>
<td style="text-align: right;">5</td>
<td style="text-align: right;">2</td>
<td style="text-align: right;">0.0000000</td>
<td style="text-align: right;">0.0000000</td>
</tr>
<tr class="even">
<td style="text-align: left;">74</td>
<td style="text-align: left;">VWA</td>
<td style="text-align: right;">15</td>
<td style="text-align: right;">4</td>
<td style="text-align: right;">0.0000000</td>
<td style="text-align: right;">0.0000000</td>
</tr>
<tr class="odd">
<td style="text-align: left;">79</td>
<td style="text-align: left;">Ig_3</td>
<td style="text-align: right;">25</td>
<td style="text-align: right;">24</td>
<td style="text-align: right;">0.0000000</td>
<td style="text-align: right;">0.0000000</td>
</tr>
<tr class="even">
<td style="text-align: left;">88</td>
<td style="text-align: left;">Transposase_21</td>
<td style="text-align: right;">3</td>
<td style="text-align: right;">3</td>
<td style="text-align: right;">0.0000000</td>
<td style="text-align: right;">0.0000000</td>
</tr>
<tr class="odd">
<td style="text-align: left;">90</td>
<td style="text-align: left;">GPS</td>
<td style="text-align: right;">7</td>
<td style="text-align: right;">5</td>
<td style="text-align: right;">0.0000000</td>
<td style="text-align: right;">0.0000000</td>
</tr>
<tr class="even">
<td style="text-align: left;">93</td>
<td style="text-align: left;">S1</td>
<td style="text-align: right;">7</td>
<td style="text-align: right;">3</td>
<td style="text-align: right;">0.0000000</td>
<td style="text-align: right;">0.0000000</td>
</tr>
<tr class="odd">
<td style="text-align: left;">104</td>
<td style="text-align: left;">Inhibitor_I29</td>
<td style="text-align: right;">10</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0.0000000</td>
<td style="text-align: right;">0.0000000</td>
</tr>
<tr class="even">
<td style="text-align: left;">114</td>
<td style="text-align: left;">cEGF</td>
<td style="text-align: right;">4</td>
<td style="text-align: right;">4</td>
<td style="text-align: right;">0.0000000</td>
<td style="text-align: right;">0.0000000</td>
</tr>
<tr class="odd">
<td style="text-align: left;">121</td>
<td style="text-align: left;">Y_phosphatase</td>
<td style="text-align: right;">5</td>
<td style="text-align: right;">5</td>
<td style="text-align: right;">0.0000000</td>
<td style="text-align: right;">0.0000000</td>
</tr>
<tr class="even">
<td style="text-align: left;">123</td>
<td style="text-align: left;">IBR</td>
<td style="text-align: right;">3</td>
<td style="text-align: right;">3</td>
<td style="text-align: right;">0.0000000</td>
<td style="text-align: right;">0.0000000</td>
</tr>
<tr class="odd">
<td style="text-align: left;">140</td>
<td style="text-align: left;">Roc</td>
<td style="text-align: right;">6</td>
<td style="text-align: right;">5</td>
<td style="text-align: right;">0.0000000</td>
<td style="text-align: right;">0.0000000</td>
</tr>
<tr class="even">
<td style="text-align: left;">142</td>
<td style="text-align: left;">Actin</td>
<td style="text-align: right;">5</td>
<td style="text-align: right;">2</td>
<td style="text-align: right;">0.0000000</td>
<td style="text-align: right;">0.0000000</td>
</tr>
<tr class="odd">
<td style="text-align: left;">145</td>
<td style="text-align: left;">Kelch_1</td>
<td style="text-align: right;">6</td>
<td style="text-align: right;">6</td>
<td style="text-align: right;">0.0000000</td>
<td style="text-align: right;">0.0000000</td>
</tr>
<tr class="even">
<td style="text-align: left;">157</td>
<td style="text-align: left;">Arf</td>
<td style="text-align: right;">3</td>
<td style="text-align: right;">3</td>
<td style="text-align: right;">0.0000000</td>
<td style="text-align: right;">0.0000000</td>
</tr>
<tr class="odd">
<td style="text-align: left;">163</td>
<td style="text-align: left;">Ig_2</td>
<td style="text-align: right;">5</td>
<td style="text-align: right;">3</td>
<td style="text-align: right;">0.0000000</td>
<td style="text-align: right;">0.0000000</td>
</tr>
<tr class="even">
<td style="text-align: left;">167</td>
<td style="text-align: left;">I-set</td>
<td style="text-align: right;">12</td>
<td style="text-align: right;">5</td>
<td style="text-align: right;">0.0000000</td>
<td style="text-align: right;">0.0000000</td>
</tr>
<tr class="odd">
<td style="text-align: left;">179</td>
<td style="text-align: left;">CARD</td>
<td style="text-align: right;">3</td>
<td style="text-align: right;">3</td>
<td style="text-align: right;">0.0000000</td>
<td style="text-align: right;">0.0000000</td>
</tr>
<tr class="even">
<td style="text-align: left;">182</td>
<td style="text-align: left;">Cu2_monooxygen</td>
<td style="text-align: right;">4</td>
<td style="text-align: right;">2</td>
<td style="text-align: right;">0.0000000</td>
<td style="text-align: right;">0.0000000</td>
</tr>
<tr class="odd">
<td style="text-align: left;">197</td>
<td style="text-align: left;">SRCR</td>
<td style="text-align: right;">25</td>
<td style="text-align: right;">19</td>
<td style="text-align: right;">0.0000000</td>
<td style="text-align: right;">0.0000000</td>
</tr>
<tr class="even">
<td style="text-align: left;">201</td>
<td style="text-align: left;">7tm_2</td>
<td style="text-align: right;">6</td>
<td style="text-align: right;">6</td>
<td style="text-align: right;">0.0000000</td>
<td style="text-align: right;">0.0000000</td>
</tr>
<tr class="odd">
<td style="text-align: left;">231</td>
<td style="text-align: left;">HTH_psq</td>
<td style="text-align: right;">7</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0.0000000</td>
<td style="text-align: right;">0.0000000</td>
</tr>
<tr class="even">
<td style="text-align: left;">270</td>
<td style="text-align: left;">Tubulin_C</td>
<td style="text-align: right;">10</td>
<td style="text-align: right;">3</td>
<td style="text-align: right;">0.0000000</td>
<td style="text-align: right;">0.0000000</td>
</tr>
<tr class="odd">
<td style="text-align: left;">274</td>
<td style="text-align: left;">Chlam_PMP</td>
<td style="text-align: right;">6</td>
<td style="text-align: right;">5</td>
<td style="text-align: right;">0.0000000</td>
<td style="text-align: right;">0.0000000</td>
</tr>
<tr class="even">
<td style="text-align: left;">278</td>
<td style="text-align: left;">Calx-beta</td>
<td style="text-align: right;">28</td>
<td style="text-align: right;">17</td>
<td style="text-align: right;">0.0000000</td>
<td style="text-align: right;">0.0000000</td>
</tr>
<tr class="odd">
<td style="text-align: left;">281</td>
<td style="text-align: left;">FXa_inhibition</td>
<td style="text-align: right;">5</td>
<td style="text-align: right;">4</td>
<td style="text-align: right;">0.0000000</td>
<td style="text-align: right;">0.0000000</td>
</tr>
<tr class="even">
<td style="text-align: left;">295</td>
<td style="text-align: left;">EGF_CA</td>
<td style="text-align: right;">3</td>
<td style="text-align: right;">3</td>
<td style="text-align: right;">0.0000000</td>
<td style="text-align: right;">0.0000000</td>
</tr>
<tr class="odd">
<td style="text-align: left;">317</td>
<td style="text-align: left;">Peptidase_A17</td>
<td style="text-align: right;">6</td>
<td style="text-align: right;">5</td>
<td style="text-align: right;">0.0000000</td>
<td style="text-align: right;">0.0000000</td>
</tr>
<tr class="even">
<td style="text-align: left;">330</td>
<td style="text-align: left;">GAIN</td>
<td style="text-align: right;">6</td>
<td style="text-align: right;">3</td>
<td style="text-align: right;">0.0000000</td>
<td style="text-align: right;">0.0000000</td>
</tr>
<tr class="odd">
<td style="text-align: left;">227</td>
<td style="text-align: left;">Pkinase</td>
<td style="text-align: right;">39</td>
<td style="text-align: right;">61</td>
<td style="text-align: right;">0.0000896</td>
<td style="text-align: right;">0.0001217</td>
</tr>
<tr class="even">
<td style="text-align: left;">256</td>
<td style="text-align: left;">NAC</td>
<td style="text-align: right;">12</td>
<td style="text-align: right;">15</td>
<td style="text-align: right;">0.0003831</td>
<td style="text-align: right;">0.0005181</td>
</tr>
<tr class="odd">
<td style="text-align: left;">29</td>
<td style="text-align: left;">rve</td>
<td style="text-align: right;">20</td>
<td style="text-align: right;">29</td>
<td style="text-align: right;">0.0006252</td>
<td style="text-align: right;">0.0008422</td>
</tr>
<tr class="even">
<td style="text-align: left;">83</td>
<td style="text-align: left;">ubiquitin</td>
<td style="text-align: right;">7</td>
<td style="text-align: right;">8</td>
<td style="text-align: right;">0.0008187</td>
<td style="text-align: right;">0.0010983</td>
</tr>
<tr class="odd">
<td style="text-align: left;">30</td>
<td style="text-align: left;">NACHT</td>
<td style="text-align: right;">11</td>
<td style="text-align: right;">15</td>
<td style="text-align: right;">0.0025496</td>
<td style="text-align: right;">0.0034063</td>
</tr>
<tr class="even">
<td style="text-align: left;">22</td>
<td style="text-align: left;">GTP_EFTU_D2</td>
<td style="text-align: right;">5</td>
<td style="text-align: right;">6</td>
<td style="text-align: right;">0.0048549</td>
<td style="text-align: right;">0.0064601</td>
</tr>
<tr class="odd">
<td style="text-align: left;">122</td>
<td style="text-align: left;">RVT_1</td>
<td style="text-align: right;">21</td>
<td style="text-align: right;">35</td>
<td style="text-align: right;">0.0076363</td>
<td style="text-align: right;">0.0101204</td>
</tr>
<tr class="even">
<td style="text-align: left;">316</td>
<td style="text-align: left;">zf-C3HC4</td>
<td style="text-align: right;">4</td>
<td style="text-align: right;">5</td>
<td style="text-align: right;">0.0118132</td>
<td style="text-align: right;">0.0155934</td>
</tr>
<tr class="odd">
<td style="text-align: left;">77</td>
<td style="text-align: left;">COR</td>
<td style="text-align: right;">9</td>
<td style="text-align: right;">14</td>
<td style="text-align: right;">0.0217390</td>
<td style="text-align: right;">0.0285811</td>
</tr>
<tr class="even">
<td style="text-align: left;">261</td>
<td style="text-align: left;">GTP_EFTU</td>
<td style="text-align: right;">5</td>
<td style="text-align: right;">7</td>
<td style="text-align: right;">0.0220190</td>
<td style="text-align: right;">0.0288344</td>
</tr>
<tr class="odd">
<td style="text-align: left;">54</td>
<td style="text-align: left;">SAM_1</td>
<td style="text-align: right;">3</td>
<td style="text-align: right;">4</td>
<td style="text-align: right;">0.0287298</td>
<td style="text-align: right;">0.0373261</td>
</tr>
<tr class="even">
<td style="text-align: left;">325</td>
<td style="text-align: left;">EF-hand_7</td>
<td style="text-align: right;">3</td>
<td style="text-align: right;">4</td>
<td style="text-align: right;">0.0287298</td>
<td style="text-align: right;">0.0373261</td>
</tr>
</tbody>
</table>

Under

<table>
<colgroup>
<col style="width: 4%" />
<col style="width: 19%" />
<col style="width: 17%" />
<col style="width: 24%" />
<col style="width: 12%" />
<col style="width: 21%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: left;"></th>
<th style="text-align: left;">Domain</th>
<th style="text-align: right;">Count_in_DEGs</th>
<th style="text-align: right;">Count_in_Background</th>
<th style="text-align: right;">p_value</th>
<th style="text-align: right;">Corrected_p_value</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">30</td>
<td style="text-align: left;">CD225</td>
<td style="text-align: right;">3</td>
<td style="text-align: right;">3</td>
<td style="text-align: right;">0.0000000</td>
<td style="text-align: right;">0.0000000</td>
</tr>
<tr class="even">
<td style="text-align: left;">56</td>
<td style="text-align: left;">Cyclin_N</td>
<td style="text-align: right;">6</td>
<td style="text-align: right;">6</td>
<td style="text-align: right;">0.0000000</td>
<td style="text-align: right;">0.0000000</td>
</tr>
<tr class="odd">
<td style="text-align: left;">67</td>
<td style="text-align: left;">Cu-oxidase_2</td>
<td style="text-align: right;">5</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0.0000000</td>
<td style="text-align: right;">0.0000000</td>
</tr>
<tr class="even">
<td style="text-align: left;">69</td>
<td style="text-align: left;">Gal-bind_lectin</td>
<td style="text-align: right;">6</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0.0000000</td>
<td style="text-align: right;">0.0000000</td>
</tr>
<tr class="odd">
<td style="text-align: left;">79</td>
<td style="text-align: left;">AAA_9</td>
<td style="text-align: right;">11</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0.0000000</td>
<td style="text-align: right;">0.0000000</td>
</tr>
<tr class="even">
<td style="text-align: left;">85</td>
<td style="text-align: left;">TPH</td>
<td style="text-align: right;">8</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0.0000000</td>
<td style="text-align: right;">0.0000000</td>
</tr>
<tr class="odd">
<td style="text-align: left;">96</td>
<td style="text-align: left;">MyTH4</td>
<td style="text-align: right;">4</td>
<td style="text-align: right;">2</td>
<td style="text-align: right;">0.0000000</td>
<td style="text-align: right;">0.0000000</td>
</tr>
<tr class="even">
<td style="text-align: left;">109</td>
<td style="text-align: left;">Carn_acyltransf</td>
<td style="text-align: right;">3</td>
<td style="text-align: right;">3</td>
<td style="text-align: right;">0.0000000</td>
<td style="text-align: right;">0.0000000</td>
</tr>
<tr class="odd">
<td style="text-align: left;">126</td>
<td style="text-align: left;">Laminin_G_2</td>
<td style="text-align: right;">7</td>
<td style="text-align: right;">2</td>
<td style="text-align: right;">0.0000000</td>
<td style="text-align: right;">0.0000000</td>
</tr>
<tr class="even">
<td style="text-align: left;">163</td>
<td style="text-align: left;">AAA_6</td>
<td style="text-align: right;">13</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0.0000000</td>
<td style="text-align: right;">0.0000000</td>
</tr>
<tr class="odd">
<td style="text-align: left;">186</td>
<td style="text-align: left;">Arm</td>
<td style="text-align: right;">5</td>
<td style="text-align: right;">5</td>
<td style="text-align: right;">0.0000000</td>
<td style="text-align: right;">0.0000000</td>
</tr>
<tr class="even">
<td style="text-align: left;">196</td>
<td style="text-align: left;">DHC_N1</td>
<td style="text-align: right;">10</td>
<td style="text-align: right;">2</td>
<td style="text-align: right;">0.0000000</td>
<td style="text-align: right;">0.0000000</td>
</tr>
<tr class="odd">
<td style="text-align: left;">202</td>
<td style="text-align: left;">ABC2_membrane</td>
<td style="text-align: right;">15</td>
<td style="text-align: right;">10</td>
<td style="text-align: right;">0.0000000</td>
<td style="text-align: right;">0.0000000</td>
</tr>
<tr class="even">
<td style="text-align: left;">221</td>
<td style="text-align: left;">FERM_M</td>
<td style="text-align: right;">8</td>
<td style="text-align: right;">5</td>
<td style="text-align: right;">0.0000000</td>
<td style="text-align: right;">0.0000000</td>
</tr>
<tr class="odd">
<td style="text-align: left;">248</td>
<td style="text-align: left;">Cyclin_C</td>
<td style="text-align: right;">6</td>
<td style="text-align: right;">3</td>
<td style="text-align: right;">0.0000000</td>
<td style="text-align: right;">0.0000000</td>
</tr>
<tr class="even">
<td style="text-align: left;">252</td>
<td style="text-align: left;">HlyIII</td>
<td style="text-align: right;">5</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0.0000000</td>
<td style="text-align: right;">0.0000000</td>
</tr>
<tr class="odd">
<td style="text-align: left;">274</td>
<td style="text-align: left;">Sortilin_C</td>
<td style="text-align: right;">3</td>
<td style="text-align: right;">3</td>
<td style="text-align: right;">0.0000000</td>
<td style="text-align: right;">0.0000000</td>
</tr>
<tr class="even">
<td style="text-align: left;">289</td>
<td style="text-align: left;">EBP</td>
<td style="text-align: right;">3</td>
<td style="text-align: right;">3</td>
<td style="text-align: right;">0.0000000</td>
<td style="text-align: right;">0.0000000</td>
</tr>
<tr class="odd">
<td style="text-align: left;">293</td>
<td style="text-align: left;">Integrin_beta</td>
<td style="text-align: right;">4</td>
<td style="text-align: right;">3</td>
<td style="text-align: right;">0.0000000</td>
<td style="text-align: right;">0.0000000</td>
</tr>
<tr class="even">
<td style="text-align: left;">305</td>
<td style="text-align: left;">ITI_HC_C</td>
<td style="text-align: right;">4</td>
<td style="text-align: right;">2</td>
<td style="text-align: right;">0.0000000</td>
<td style="text-align: right;">0.0000000</td>
</tr>
<tr class="odd">
<td style="text-align: left;">309</td>
<td style="text-align: left;">polyprenyl_synt</td>
<td style="text-align: right;">5</td>
<td style="text-align: right;">3</td>
<td style="text-align: right;">0.0000000</td>
<td style="text-align: right;">0.0000000</td>
</tr>
<tr class="even">
<td style="text-align: left;">323</td>
<td style="text-align: left;">Tektin</td>
<td style="text-align: right;">6</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0.0000000</td>
<td style="text-align: right;">0.0000000</td>
</tr>
<tr class="odd">
<td style="text-align: left;">330</td>
<td style="text-align: left;">ADK</td>
<td style="text-align: right;">5</td>
<td style="text-align: right;">3</td>
<td style="text-align: right;">0.0000000</td>
<td style="text-align: right;">0.0000000</td>
</tr>
<tr class="even">
<td style="text-align: left;">344</td>
<td style="text-align: left;">G-alpha</td>
<td style="text-align: right;">4</td>
<td style="text-align: right;">4</td>
<td style="text-align: right;">0.0000000</td>
<td style="text-align: right;">0.0000000</td>
</tr>
<tr class="odd">
<td style="text-align: left;">347</td>
<td style="text-align: left;">Histone</td>
<td style="text-align: right;">5</td>
<td style="text-align: right;">3</td>
<td style="text-align: right;">0.0000000</td>
<td style="text-align: right;">0.0000000</td>
</tr>
<tr class="even">
<td style="text-align: left;">354</td>
<td style="text-align: left;">MORN</td>
<td style="text-align: right;">6</td>
<td style="text-align: right;">5</td>
<td style="text-align: right;">0.0000000</td>
<td style="text-align: right;">0.0000000</td>
</tr>
<tr class="odd">
<td style="text-align: left;">357</td>
<td style="text-align: left;">Kazal_2</td>
<td style="text-align: right;">8</td>
<td style="text-align: right;">3</td>
<td style="text-align: right;">0.0000000</td>
<td style="text-align: right;">0.0000000</td>
</tr>
<tr class="even">
<td style="text-align: left;">374</td>
<td style="text-align: left;">7tm_2</td>
<td style="text-align: right;">20</td>
<td style="text-align: right;">18</td>
<td style="text-align: right;">0.0000000</td>
<td style="text-align: right;">0.0000000</td>
</tr>
<tr class="odd">
<td style="text-align: left;">387</td>
<td style="text-align: left;">AAA_7</td>
<td style="text-align: right;">13</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0.0000000</td>
<td style="text-align: right;">0.0000000</td>
</tr>
<tr class="even">
<td style="text-align: left;">395</td>
<td style="text-align: left;">p450</td>
<td style="text-align: right;">18</td>
<td style="text-align: right;">9</td>
<td style="text-align: right;">0.0000000</td>
<td style="text-align: right;">0.0000000</td>
</tr>
<tr class="odd">
<td style="text-align: left;">404</td>
<td style="text-align: left;">SHIPPO-rpt</td>
<td style="text-align: right;">6</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0.0000000</td>
<td style="text-align: right;">0.0000000</td>
</tr>
<tr class="even">
<td style="text-align: left;">405</td>
<td style="text-align: left;">Dynein_heavy</td>
<td style="text-align: right;">14</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0.0000000</td>
<td style="text-align: right;">0.0000000</td>
</tr>
<tr class="odd">
<td style="text-align: left;">421</td>
<td style="text-align: left;">Cu-oxidase</td>
<td style="text-align: right;">5</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0.0000000</td>
<td style="text-align: right;">0.0000000</td>
</tr>
<tr class="even">
<td style="text-align: left;">432</td>
<td style="text-align: left;">Dpy-30</td>
<td style="text-align: right;">7</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0.0000000</td>
<td style="text-align: right;">0.0000000</td>
</tr>
<tr class="odd">
<td style="text-align: left;">446</td>
<td style="text-align: left;">AAA_8</td>
<td style="text-align: right;">12</td>
<td style="text-align: right;">2</td>
<td style="text-align: right;">0.0000000</td>
<td style="text-align: right;">0.0000000</td>
</tr>
<tr class="even">
<td style="text-align: left;">456</td>
<td style="text-align: left;">FERM_C</td>
<td style="text-align: right;">3</td>
<td style="text-align: right;">3</td>
<td style="text-align: right;">0.0000000</td>
<td style="text-align: right;">0.0000000</td>
</tr>
<tr class="odd">
<td style="text-align: left;">491</td>
<td style="text-align: left;">IQ</td>
<td style="text-align: right;">14</td>
<td style="text-align: right;">6</td>
<td style="text-align: right;">0.0000000</td>
<td style="text-align: right;">0.0000000</td>
</tr>
<tr class="even">
<td style="text-align: left;">503</td>
<td style="text-align: left;">Abhydrolase_1</td>
<td style="text-align: right;">4</td>
<td style="text-align: right;">2</td>
<td style="text-align: right;">0.0000000</td>
<td style="text-align: right;">0.0000000</td>
</tr>
<tr class="odd">
<td style="text-align: left;">527</td>
<td style="text-align: left;">CHGN</td>
<td style="text-align: right;">6</td>
<td style="text-align: right;">3</td>
<td style="text-align: right;">0.0000000</td>
<td style="text-align: right;">0.0000000</td>
</tr>
<tr class="even">
<td style="text-align: left;">554</td>
<td style="text-align: left;">FERM_N</td>
<td style="text-align: right;">4</td>
<td style="text-align: right;">2</td>
<td style="text-align: right;">0.0000000</td>
<td style="text-align: right;">0.0000000</td>
</tr>
<tr class="odd">
<td style="text-align: left;">563</td>
<td style="text-align: left;">Kinesin</td>
<td style="text-align: right;">13</td>
<td style="text-align: right;">9</td>
<td style="text-align: right;">0.0000000</td>
<td style="text-align: right;">0.0000000</td>
</tr>
<tr class="even">
<td style="text-align: left;">589</td>
<td style="text-align: left;">ELO</td>
<td style="text-align: right;">6</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0.0000000</td>
<td style="text-align: right;">0.0000000</td>
</tr>
<tr class="odd">
<td style="text-align: left;">595</td>
<td style="text-align: left;">RYDR_ITPR</td>
<td style="text-align: right;">5</td>
<td style="text-align: right;">5</td>
<td style="text-align: right;">0.0000000</td>
<td style="text-align: right;">0.0000000</td>
</tr>
<tr class="even">
<td style="text-align: left;">604</td>
<td style="text-align: left;">Forkhead</td>
<td style="text-align: right;">7</td>
<td style="text-align: right;">4</td>
<td style="text-align: right;">0.0000000</td>
<td style="text-align: right;">0.0000000</td>
</tr>
<tr class="odd">
<td style="text-align: left;">605</td>
<td style="text-align: left;">zf-MYND</td>
<td style="text-align: right;">6</td>
<td style="text-align: right;">5</td>
<td style="text-align: right;">0.0000000</td>
<td style="text-align: right;">0.0000000</td>
</tr>
<tr class="even">
<td style="text-align: left;">664</td>
<td style="text-align: left;">DHC_N2</td>
<td style="text-align: right;">15</td>
<td style="text-align: right;">2</td>
<td style="text-align: right;">0.0000000</td>
<td style="text-align: right;">0.0000000</td>
</tr>
<tr class="odd">
<td style="text-align: left;">683</td>
<td style="text-align: left;">Ins145_P3_rec</td>
<td style="text-align: right;">3</td>
<td style="text-align: right;">3</td>
<td style="text-align: right;">0.0000000</td>
<td style="text-align: right;">0.0000000</td>
</tr>
<tr class="even">
<td style="text-align: left;">696</td>
<td style="text-align: left;">Laminin_G_1</td>
<td style="text-align: right;">8</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0.0000000</td>
<td style="text-align: right;">0.0000000</td>
</tr>
<tr class="odd">
<td style="text-align: left;">700</td>
<td style="text-align: left;">Y_phosphatase</td>
<td style="text-align: right;">17</td>
<td style="text-align: right;">15</td>
<td style="text-align: right;">0.0000000</td>
<td style="text-align: right;">0.0000000</td>
</tr>
<tr class="even">
<td style="text-align: left;">716</td>
<td style="text-align: left;">Collagen</td>
<td style="text-align: right;">20</td>
<td style="text-align: right;">19</td>
<td style="text-align: right;">0.0000000</td>
<td style="text-align: right;">0.0000000</td>
</tr>
<tr class="odd">
<td style="text-align: left;">718</td>
<td style="text-align: left;">TTL</td>
<td style="text-align: right;">7</td>
<td style="text-align: right;">3</td>
<td style="text-align: right;">0.0000000</td>
<td style="text-align: right;">0.0000000</td>
</tr>
<tr class="even">
<td style="text-align: left;">735</td>
<td style="text-align: left;">EGF_CA</td>
<td style="text-align: right;">10</td>
<td style="text-align: right;">9</td>
<td style="text-align: right;">0.0000000</td>
<td style="text-align: right;">0.0000000</td>
</tr>
<tr class="odd">
<td style="text-align: left;">748</td>
<td style="text-align: left;">Laminin_EGF</td>
<td style="text-align: right;">3</td>
<td style="text-align: right;">3</td>
<td style="text-align: right;">0.0000000</td>
<td style="text-align: right;">0.0000000</td>
</tr>
<tr class="even">
<td style="text-align: left;">755</td>
<td style="text-align: left;">Peptidase_M14</td>
<td style="text-align: right;">4</td>
<td style="text-align: right;">2</td>
<td style="text-align: right;">0.0000000</td>
<td style="text-align: right;">0.0000000</td>
</tr>
<tr class="odd">
<td style="text-align: left;">759</td>
<td style="text-align: left;">FA_desaturase</td>
<td style="text-align: right;">3</td>
<td style="text-align: right;">3</td>
<td style="text-align: right;">0.0000000</td>
<td style="text-align: right;">0.0000000</td>
</tr>
<tr class="even">
<td style="text-align: left;">402</td>
<td style="text-align: left;">Calx-beta</td>
<td style="text-align: right;">50</td>
<td style="text-align: right;">53</td>
<td style="text-align: right;">0.0000000</td>
<td style="text-align: right;">0.0000000</td>
</tr>
<tr class="odd">
<td style="text-align: left;">376</td>
<td style="text-align: left;">EGF</td>
<td style="text-align: right;">34</td>
<td style="text-align: right;">40</td>
<td style="text-align: right;">0.0000000</td>
<td style="text-align: right;">0.0000000</td>
</tr>
<tr class="even">
<td style="text-align: left;">254</td>
<td style="text-align: left;">ig</td>
<td style="text-align: right;">22</td>
<td style="text-align: right;">25</td>
<td style="text-align: right;">0.0000000</td>
<td style="text-align: right;">0.0000000</td>
</tr>
<tr class="odd">
<td style="text-align: left;">122</td>
<td style="text-align: left;">Ig_3</td>
<td style="text-align: right;">48</td>
<td style="text-align: right;">82</td>
<td style="text-align: right;">0.0000000</td>
<td style="text-align: right;">0.0000000</td>
</tr>
<tr class="even">
<td style="text-align: left;">443</td>
<td style="text-align: left;">AAA</td>
<td style="text-align: right;">33</td>
<td style="text-align: right;">57</td>
<td style="text-align: right;">0.0000017</td>
<td style="text-align: right;">0.0000028</td>
</tr>
<tr class="odd">
<td style="text-align: left;">435</td>
<td style="text-align: left;">MT</td>
<td style="text-align: right;">14</td>
<td style="text-align: right;">18</td>
<td style="text-align: right;">0.0000031</td>
<td style="text-align: right;">0.0000051</td>
</tr>
<tr class="even">
<td style="text-align: left;">334</td>
<td style="text-align: left;">TPR_8</td>
<td style="text-align: right;">11</td>
<td style="text-align: right;">16</td>
<td style="text-align: right;">0.0002040</td>
<td style="text-align: right;">0.0003396</td>
</tr>
<tr class="odd">
<td style="text-align: left;">506</td>
<td style="text-align: left;">Sushi</td>
<td style="text-align: right;">18</td>
<td style="text-align: right;">32</td>
<td style="text-align: right;">0.0003727</td>
<td style="text-align: right;">0.0006190</td>
</tr>
<tr class="even">
<td style="text-align: left;">680</td>
<td style="text-align: left;">F-box-like</td>
<td style="text-align: right;">10</td>
<td style="text-align: right;">15</td>
<td style="text-align: right;">0.0005295</td>
<td style="text-align: right;">0.0008776</td>
</tr>
<tr class="odd">
<td style="text-align: left;">294</td>
<td style="text-align: left;">Cadherin</td>
<td style="text-align: right;">18</td>
<td style="text-align: right;">33</td>
<td style="text-align: right;">0.0006387</td>
<td style="text-align: right;">0.0010563</td>
</tr>
<tr class="even">
<td style="text-align: left;">635</td>
<td style="text-align: left;">fn3</td>
<td style="text-align: right;">24</td>
<td style="text-align: right;">48</td>
<td style="text-align: right;">0.0007438</td>
<td style="text-align: right;">0.0012276</td>
</tr>
<tr class="odd">
<td style="text-align: left;">703</td>
<td style="text-align: left;">PID</td>
<td style="text-align: right;">6</td>
<td style="text-align: right;">8</td>
<td style="text-align: right;">0.0010951</td>
<td style="text-align: right;">0.0018035</td>
</tr>
<tr class="even">
<td style="text-align: left;">361</td>
<td style="text-align: left;">AAA_5</td>
<td style="text-align: right;">4</td>
<td style="text-align: right;">5</td>
<td style="text-align: right;">0.0021503</td>
<td style="text-align: right;">0.0035191</td>
</tr>
<tr class="odd">
<td style="text-align: left;">393</td>
<td style="text-align: left;">ABC2_membrane_3</td>
<td style="text-align: right;">4</td>
<td style="text-align: right;">5</td>
<td style="text-align: right;">0.0021503</td>
<td style="text-align: right;">0.0035191</td>
</tr>
<tr class="even">
<td style="text-align: left;">441</td>
<td style="text-align: left;">cNMP_binding</td>
<td style="text-align: right;">4</td>
<td style="text-align: right;">5</td>
<td style="text-align: right;">0.0021503</td>
<td style="text-align: right;">0.0035191</td>
</tr>
<tr class="odd">
<td style="text-align: left;">148</td>
<td style="text-align: left;">GPS</td>
<td style="text-align: right;">10</td>
<td style="text-align: right;">17</td>
<td style="text-align: right;">0.0025973</td>
<td style="text-align: right;">0.0042417</td>
</tr>
<tr class="even">
<td style="text-align: left;">103</td>
<td style="text-align: left;">AIG1</td>
<td style="text-align: right;">5</td>
<td style="text-align: right;">7</td>
<td style="text-align: right;">0.0032993</td>
<td style="text-align: right;">0.0053769</td>
</tr>
<tr class="odd">
<td style="text-align: left;">290</td>
<td style="text-align: left;">F-box</td>
<td style="text-align: right;">12</td>
<td style="text-align: right;">22</td>
<td style="text-align: right;">0.0033969</td>
<td style="text-align: right;">0.0055244</td>
</tr>
<tr class="even">
<td style="text-align: left;">679</td>
<td style="text-align: left;">hEGF</td>
<td style="text-align: right;">7</td>
<td style="text-align: right;">11</td>
<td style="text-align: right;">0.0036150</td>
<td style="text-align: right;">0.0058546</td>
</tr>
<tr class="odd">
<td style="text-align: left;">730</td>
<td style="text-align: left;">DSPc</td>
<td style="text-align: right;">7</td>
<td style="text-align: right;">11</td>
<td style="text-align: right;">0.0036150</td>
<td style="text-align: right;">0.0058546</td>
</tr>
<tr class="even">
<td style="text-align: left;">10</td>
<td style="text-align: left;">SRCR</td>
<td style="text-align: right;">23</td>
<td style="text-align: right;">51</td>
<td style="text-align: right;">0.0053630</td>
<td style="text-align: right;">0.0086677</td>
</tr>
<tr class="odd">
<td style="text-align: left;">286</td>
<td style="text-align: left;">Fibrinogen_C</td>
<td style="text-align: right;">12</td>
<td style="text-align: right;">23</td>
<td style="text-align: right;">0.0057367</td>
<td style="text-align: right;">0.0092524</td>
</tr>
<tr class="even">
<td style="text-align: left;">609</td>
<td style="text-align: left;">Cyt-b5</td>
<td style="text-align: right;">3</td>
<td style="text-align: right;">4</td>
<td style="text-align: right;">0.0073492</td>
<td style="text-align: right;">0.0118286</td>
</tr>
<tr class="odd">
<td style="text-align: left;">500</td>
<td style="text-align: left;">Myosin_head</td>
<td style="text-align: right;">7</td>
<td style="text-align: right;">12</td>
<td style="text-align: right;">0.0080657</td>
<td style="text-align: right;">0.0129550</td>
</tr>
<tr class="even">
<td style="text-align: left;">776</td>
<td style="text-align: left;">DOMON</td>
<td style="text-align: right;">4</td>
<td style="text-align: right;">6</td>
<td style="text-align: right;">0.0097570</td>
<td style="text-align: right;">0.0156393</td>
</tr>
<tr class="odd">
<td style="text-align: left;">350</td>
<td style="text-align: left;">LRR_9</td>
<td style="text-align: right;">5</td>
<td style="text-align: right;">8</td>
<td style="text-align: right;">0.0099121</td>
<td style="text-align: right;">0.0158553</td>
</tr>
<tr class="even">
<td style="text-align: left;">682</td>
<td style="text-align: left;">Ig_2</td>
<td style="text-align: right;">9</td>
<td style="text-align: right;">17</td>
<td style="text-align: right;">0.0105448</td>
<td style="text-align: right;">0.0168328</td>
</tr>
<tr class="odd">
<td style="text-align: left;">342</td>
<td style="text-align: left;">Ank_4</td>
<td style="text-align: right;">8</td>
<td style="text-align: right;">15</td>
<td style="text-align: right;">0.0128642</td>
<td style="text-align: right;">0.0204515</td>
</tr>
<tr class="even">
<td style="text-align: left;">545</td>
<td style="text-align: left;">SAM_1</td>
<td style="text-align: right;">8</td>
<td style="text-align: right;">15</td>
<td style="text-align: right;">0.0128642</td>
<td style="text-align: right;">0.0204515</td>
</tr>
<tr class="odd">
<td style="text-align: left;">452</td>
<td style="text-align: left;">I-set</td>
<td style="text-align: right;">20</td>
<td style="text-align: right;">47</td>
<td style="text-align: right;">0.0177136</td>
<td style="text-align: right;">0.0281037</td>
</tr>
<tr class="even">
<td style="text-align: left;">409</td>
<td style="text-align: left;">HRM</td>
<td style="text-align: right;">5</td>
<td style="text-align: right;">9</td>
<td style="text-align: right;">0.0223882</td>
<td style="text-align: right;">0.0354481</td>
</tr>
<tr class="odd">
<td style="text-align: left;">168</td>
<td style="text-align: left;">TPR_12</td>
<td style="text-align: right;">4</td>
<td style="text-align: right;">7</td>
<td style="text-align: right;">0.0259011</td>
<td style="text-align: right;">0.0395627</td>
</tr>
<tr class="even">
<td style="text-align: left;">371</td>
<td style="text-align: left;">SAM_2</td>
<td style="text-align: right;">4</td>
<td style="text-align: right;">7</td>
<td style="text-align: right;">0.0259011</td>
<td style="text-align: right;">0.0395627</td>
</tr>
<tr class="odd">
<td style="text-align: left;">383</td>
<td style="text-align: left;">DEP</td>
<td style="text-align: right;">4</td>
<td style="text-align: right;">7</td>
<td style="text-align: right;">0.0259011</td>
<td style="text-align: right;">0.0395627</td>
</tr>
<tr class="even">
<td style="text-align: left;">510</td>
<td style="text-align: left;">HMG_box</td>
<td style="text-align: right;">3</td>
<td style="text-align: right;">5</td>
<td style="text-align: right;">0.0281449</td>
<td style="text-align: right;">0.0428221</td>
</tr>
<tr class="odd">
<td style="text-align: left;">535</td>
<td style="text-align: left;">SET</td>
<td style="text-align: right;">3</td>
<td style="text-align: right;">5</td>
<td style="text-align: right;">0.0281449</td>
<td style="text-align: right;">0.0428221</td>
</tr>
<tr class="even">
<td style="text-align: left;">470</td>
<td style="text-align: left;">ABC_tran</td>
<td style="text-align: right;">10</td>
<td style="text-align: right;">22</td>
<td style="text-align: right;">0.0325409</td>
<td style="text-align: right;">0.0494139</td>
</tr>
</tbody>
</table>

# Discussion

# Acknowledgements

### Conflict of interest

None declared.

### Author contributions

Sergio Vargas designed the study, conducted the experiments and analyzed
the data. Gert Wörheide contributed reagents. Sergio Vargas and Gert
Wörheide wrote the manuscript. Sergio Vargas manages the [project
repository](https://gitlab.lrz.de/cbas/CBAS_Transcriptome).

### Disclaimer

None of the products or companies listed are endorsed or recommended in
anyway by the author(s) of this text.

### Scripts and Data availability

All scripts used to analyze the data, as well as some miscellaneous
scripts used to, for instance, prepare the annotation table or generate
GO-term/Pfam input files for the enrichment analyses can be found in the
[project repository](https://gitlab.lrz.de/cbas/CBAS_Transcriptome/).

# References and Notes

[1] I have (repeatedly) failed to prepare libraries for Next Generation
Sequencing from bleached tissue, probably due to the presence of
secondary metabolites in the extracts.

[2] The 10 mm hole puncher has been replaced by a 6 mm version. [Click
here to see the
product.](http://www.sigmaaldrich.com/catalog/product/sigma/z708909?lang=de&region=DE)

[3] We, in fact, keep a permanent culture of “bleached” explants that
are allowed to bleach for at least 12 weeks, are fixed in liquid
nitrogen and stored at -80 °C.

[4] Zymo Research. [Click here to see the product
website.](https://www.zymoresearch.de/rna/dna-rna-co-purification/cells-tissue-rna/zr-duet-dna-rna-miniprep)

[5] EMBL GeneCore.
<http://genecore3.genecore.embl.de/genecore3/index.cfm>

[6] FastQC. <http://www.bioinformatics.babraham.ac.uk/projects/fastqc/>

[7] BioLite: <https://bitbucket.org/caseywdunn/biolite/overview>

[8] Downloaded early 2015. [Uniprot
download.](http://www.uniprot.org/downloads)

[9] Aqu2 isoforms (proteins) [*Amphimedon queenslandica* transcriptome
resource.](http://amphimedon.qcloud.qcif.edu.au/downloads.html)

[10] Blast was run from [Galaxy](https://galaxyproject.org/) and the XML
file was converted to a 25 column table by the
[ncbi\_blast\_plus](http://toolshed.g2.bx.psu.edu/view/devteam/ncbi_blast_plus)
tool.

[11] Gene Ontology Consortium. <http://geneontology.org/>

[12] The UNIPROT accession code of the blast best match associated to
each transcript in the CBAS transcriptome was used to query the GO
annotations associated with the UNIPROT protein. These GO annotations
were used to annotated the CBAS transcripts; note that these are
annotations based on annotations and should be taken with precaution.

[13] TopGO
[(https://bioconductor.org/packages/release/bioc/html/topGO.html)](https://bioconductor.org/packages/release/bioc/html/topGO.html)
expects GO terms in tab separated {Trascript, GoAnnotation} pairs. The
script produces output with a leading column indicating the UNIPROT
accession code used for each transcript, which comes handy when
controling the correct annotation was used for each transcript and can
be easily removed for further analyses.

[14] Transdecoder’s Github Repository can be found
[here.](https://transdecoder.github.io/)

[15] I honestly do not know what the purpose of this blast run was, I
did it for sake of completeness and because the reads were not filtered
before assembling the conting. Yet, after seeing the results, it seems
as if not too many contigs matched the bacterial database…

[16] Parra et al. 2007. CEGMA: a pipeline to accurately annotate core
genes in eukaryotic genomes. [Bioinformatics 23 (9):
1061-1067.](http://dx.doi.org/10.1093/bioinformatics/btm071)

[17] Francis et al. 2013. A comparison across non-model animals suggests
an optimal sequencing depth for *de novo* transcriptome assembly. [BMC
Genomics 14:167](http://dx.doi.org/10.1186/1471-2164-14-167)

[18] the script is also provided as part of this repository. The
Copyright (c) of this script belongs trinityrnaseq (2014), who reserves
all rights upon the code. [See the full LICENSE
here.](https://github.com/trinityrnaseq/trinityrnaseq/blob/master/LICENSE)

[19] With these graphs were produced for transcripts that could be
successfully translated using **Transdecoder**. In the case of the
**Uniprot** and **Aqu2** annotations, the transcripts were directly
blasted using blastn. Thus could be possible to obtain **Uniprot** and
**Aqu2** annotations for transcripts that could not be translated with
**Transdecoder**. This is in fact the case, however the number of
transcripts that could not *transdecoded* but were annotated is
relatively small. In the case of **Uniprot**, the number of transcripts
that yielded annotations but were not *transdecoded* was **7535**. For
**Aqu2**, only **11915** not *transdecoded* transcripts could be
annotated.

[20] Here it is worth noting that the evalue cutoff used for blast
against the Uniprot and Aqu2 databases was 0.001. So the annotations
obtained had an evalue at least two orders of magnitud smaller than the
set cutoff evalue.
